﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate_planet : MonoBehaviour {
    public float rotation;
    public bool isRevolve;
    public float revolve;
    public GameObject center;
    // Use this for initialization
    Vector3 diff, rotAxis;

    private void Awake()
    {
        diff = center.transform.position - gameObject.transform.position;
        diff = Vector3.Normalize(diff);
        rotAxis = (transform.up) - Vector3.Dot(transform.up, diff) * diff;
        Debug.Log("vectors");
        Debug.Log(transform.up);
    }

    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.up * Time.deltaTime * rotation);
        if(isRevolve)
            transform.RotateAround(center.transform.position, rotAxis, revolve * Time.deltaTime);
    }
}
