﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kino;

public class osc_test_recv : MonoBehaviour {

    public OSC osc;
    public rotate rotty;

    //Color m_MouseOverColor = Color.red;
    //This stores the GameObject’s original color
    //Color m_OriginalColor;
    //MeshRenderer m_Renderer;

    public Mirror mirror;

    bool recv = false;

    // Use this for initialization
    void Start () {
        osc.SetAddressHandler("/bilbo", getBilbo);
        osc.SetAddressHandler("/bilbo2", getBilbo2);
        //m_Renderer = GetComponent<MeshRenderer>();
        //m_OriginalColor = m_Renderer.material.color;
    }

    int k;
	// Update is called once per frame
	void Update () {
        
        //m_Renderer.material.color = Color.Lerp(m_Renderer.material.color, m_OriginalColor, k/99f);
        k++;
        k %= 100;
       
        
        
        //else
        //transform.Rotate(Vector3.up * rot);

    }

    void FixedUpdate()
    {            
        recv = false;
        //Debug.Log(recv);

    }

    int rot;
    void getBilbo(OscMessage message)
    {
        float x = message.GetFloat(0);
        Debug.Log(x);
        //m_Renderer.material.color = m_MouseOverColor;
        //mirror._roll+=12;
        rotty.zRot += 3;
        rotty.zRot %= 100;
        rot++;
        rot %= 2;
        recv = true;
    }

    void getBilbo2(OscMessage message)
    {
        float x = message.GetFloat(0);
        Debug.Log("hey:"+x);
        //m_Renderer.material.color = m_MouseOverColor;
        mirror._offset -= 12;
    }
}
