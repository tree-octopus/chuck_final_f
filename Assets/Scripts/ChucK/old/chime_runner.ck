// OSC stuff
// --------------------- //
OscIn oin;
OscOut out;
OscMsg msg;

50000 => int inPort;
6969 => int outPort;

inPort => oin.port;
"/numChimes, f" => string numEvent;
oin.addAddress(numEvent);
int numChimes;
chout <= numEvent <= IO.newline();

("localhost", outPort) => out.dest;
// --------------------- //
chout <= "variables declared" <= IO.newline();

0 => int received;

// receive chime stuff
// --------------------- //
while( received == 0 )
{
    chout <= "in loop " <= IO.newline();
    
    oin => now;
    
    chout <= "got something" <= IO.newline();
    while( oin.recv(msg) != 0 )
    {
        chout <= "wow" <= IO.newline();
        msg.getFloat(0) $int => numChimes;
        chout <= numChimes <= IO.newline();
        1 => received;
    }    
}

chout <= "now to do more things" <= IO.newline();
out.start("/ARG");
2 => out.add;
out.send();
// more chime stuff
float chimeTunes[numChimes];
float chimeRoot;
"/chimeTunes, f" => string tunesEvent;
for( 0 => int i; i < numChimes; i++ )
{
    "f" +=> tunesEvent;
}
chout <= tunesEvent <= IO.newline();

0 => received;
oin.addAddress(tunesEvent);

/*
OscRecv recv;
inPort => recv.port;
recv.listen();
recv.event( tunesEvent ) @=> OscEvent oe;

/*
while( received == 0 )
{
    chout <= "dude.." <= IO.newline ();
    
    oe => now;
    
    chout <= "got something again" <= IO.newline();
    while ( oe.nextMsg() != 0 )
    {
        chout <= "wow" <= IO.newline();
        oe.getFloat() => float billy;
        chout <= billy <= IO.newline();
        1 => received;
    }    
}
*/

//new OscMsg @=> msg;
while( received == 0 )
{
    chout <= "in loop 2" <= IO.newline();    
    oin => now;    
    chout <= "got something" <= IO.newline();
    while( oin.recv(msg) != 0 )
    {
        chout <= "wow" <= IO.newline();
        msg.getFloat(0) => chimeRoot;
        
        for( 0 => int i; i < numChimes; i++ )
        {
            
            msg.getFloat(i+1) => chimeTunes[i];
            chout <= chimeTunes[i] <= IO.newline();
        }
        
        chout <= chimeRoot <= IO.newline();
        1 => received;
    }  
    
}

chout <= "wow it actually worked" <= IO.newline();