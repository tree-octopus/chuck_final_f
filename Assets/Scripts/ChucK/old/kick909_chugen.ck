class Kick909 extends Chugen    
{
    // the base frequency
    60 =>  float freq;
    // phase junk
    pi/2. => float phase;
    // counter variable
    float p;
    
    // pitch envelope
    ExpEnv pitch_env => blackhole;
    (0::ms, 80::ms, 0, 0::ms) => pitch_env.set;
    // amplitude envelope
    ExpEnv amp_env => blackhole;
    (2.2::ms, 6::ms, 0.5, 440::ms) => amp_env.set;
    // sustain for 60 ms
    60 => float sus_time;
    0 => int didSus;
    second/samp => float srate;
    
    float temp;
    float samp_old;
    
    fun float tick ( float xn )
    {
        
        //t*srate => float N;
        
        //<<< "p:", p, "later:", amp_env.dec_later/samp >>>;
        if((p >= amp_env.dec_later/samp + (sus_time::ms)/samp) && (didSus == 0))
        {
            //<<< "if" >>>;
            //1 => amp_env.setKeyOff;        
            //1 => didSus;
            1 => noteOff;
        }
        else{
            //<<< "p:", p, "later:", amp_env.dec_later/samp >>>;
            //<<< "elsed" >>>;
        }
        //if(amp_env.getState() == ExpEnv.SUSTAIN && p - samp_old >= sus_time)
            //1 => amp_env.setKeyOff;        
        
        amp_env.value() * Math.sin(2 * pi * 
                    ((p) * freq/srate + pitch_env.value() * 8) + phase) => temp;
        
        1 +=> p;
        //<<< temp >>>;
        //if(temp > 0)
        //    <<< "not 0:", temp >>>;
        //return amp_env.value() * Math.sin(2 * pi * ((p++)*pitch_env.value() * freq/srate) + phase);
        return temp;
    }
    
    fun void noteOn(int no)
    {
        no => pitch_env.setKeyOn;
        no => amp_env.setKeyOn;
        now/samp => p;
        0 => didSus;
    }
    
    fun void noteOff(int no)
    {
        no => amp_env.setKeyOff;
        1 => didSus;
    }
}

Kick909 k => dac;
1 => k.noteOn;
2::second => now;