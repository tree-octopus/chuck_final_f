public class Chime extends Chubgraph 
{
    BandedWG bwg => outlet;
    ModalBar bar => outlet;
    
    0.1 => bar.damp;
    0.9 => bwg.modesGain;
    
    3 => bar.mode;
    3 => bar.modeRatio;
    .34 => bar.modeRadius;
    0.91 => bwg.bowPressure;
    
    3 => bwg.preset;
    4 => bar.preset;
    
    8000 => float lengthParam;
    
    fun void setFreq(float f)
    {
        f => bwg.freq;
        f/2 => bar.freq;
    }
    
    fun float getFreq()
    {
        return bwg.freq();
    }
    
    fun float getLength() {
        return Math.sqrt(lengthParam / bwg.freq());
    }
    
    fun void play(float vel, float strikePos) 
    {
        strikePos => bwg.strikePosition;
        strikePos => bar.strikePosition;
        
        vel => bwg.pluck;
        vel => bar.strike;
        
        10::second => now;
    }
}

