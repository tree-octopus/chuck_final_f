//-----------------------//

OscRecv recv;
6161 => int inPort => recv.port;
recv.listen();

OscSend send;
//OscOut send;
6969 => int outPort;
send.setHost("localhost", outPort);
chout <= "AAAHH " <= IO.newline();
//-----------------------//
"/numChimes, f" => string numEvent;
int numChimes;

recv.event(numEvent) @=> OscEvent oe;
float tune;

//<<< "got something" >>>;

chout <= numEvent <= IO.newline();
0 => int received;
int k;
//-----------------------//
while(received == 0)
{
    chout <= "waiting...." <= IO.newline();
    oe => now;
    while( oe.nextMsg() != 0 )
    {
        if( k == 0 )
        {
            chout <= "getting OSC stuff?" <= IO.newline();
            oe.getFloat()$int=> numChimes;
            1 => received;
            <<< "num chimes: ", numChimes >>>;
        }
        else
        {
            <<< "also got this int: ", oe.getFloat() >>>;
        }  
        k++;
        send.startMsg("/ARG, f");
        2 => send.addFloat;
    }
}



<<< "gonna do more stuff now" >>>;
<<< "num chimes: ", numChimes >>>;

"/chimeTunes, " => string tunesEvent;
"f" +=> tunesEvent;
for(0 => int i; i < numChimes; i++) {
    "f" +=> tunesEvent;
}
<<< tunesEvent >>>;
/*
OscIn oin;
OscMsg msg;
inPort => oin.port;
oin.addAddress(tunesEvent);
while ( true )
{
    // wait for event to arrive
    oin => now;

    // grab the next message from the queue. 
    while ( oin.recv(msg) != 0 )
    { 
        // getFloat fetches the expected float (as indicated by "f")
        msg.getFloat(0) => float billy;
        // print
        <<< "got (via OSC):", billy >>>;
        
    }
}

*/
//recv.event(tunesEvent) @=> oe;
//recv.listen();
/*
while(true) {
   oe => now;
    <<< "got something" >>>;

    while( oe.nextMsg() != 0 )
    {
        <<< "got this int: ", oe.getFloat() >>>;
        //(oe.getFloat())$int => numChimes; 
    }    
}
