public class Kick909 extends Chubgraph
{
    SinOsc osc => ADSR env => outlet;
    60 => osc.freq;
    
    
    (5::ms, 50::ms, 0, 0::ms) => env.set;
    
    
    fun void exp() 
    {
        
    }
    
    fun void play()
    {
        1 => env.keyOn;
    }
    
}

Kick909 => dac;

2::second=> now;