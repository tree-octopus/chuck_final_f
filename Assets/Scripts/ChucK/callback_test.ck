//external float tester[];
//external float test;
float tester[];
float test;

OscSend send;
OscOut sender;
//OscOut send;
send.setHost("localhost", 6969);
//send.dest("localhost", 6969);
[1., 4., 5.] @=> tester;

chout <= "why the hell" <= IO.newline();
send.startMsg("/test, fff");
//send.start("/test");
for(0 => int i; i < tester.cap(); i++) {
    tester[i] => test => send.addFloat;
    //tester[i] => test => send.add;
    //chout <= tester[i] <= IO.newline();
}

//send.send();
//56.9 => test;

1::second => now;