fun float db2lin ( float db )
{
    return Math.pow(10, db/20);
}

class EnvelopeFollower extends Chugen
{
    float envelope;
    float b0, a1;
    float yn_1;
    
    -0.9995 => a1;
    1-Math.fabs(a1) => b0; //calculate correct b0 whether a1 is + or -
    
    fun float tick ( float xn )
    {
        //make signal positive only
        Math.fabs(xn) => xn;
        
        //low pass filter it
        b0*xn - a1*yn_1 => float yn;
        
        //store previous sample values
        yn => yn_1;
        
        
        yn => envelope; 
        
        //set envelope variables
        return yn;
        
    }
}



public class Compressor extends Chugen
{
    
    EnvelopeFollower attack;
    EnvelopeFollower release;
    
    attackTime(0.005);
    0.5 => float threshold; //linear threshold
    10 => float ratio;
    0 => float level;
    
    fun float tick ( float xn )
    {
        //compute level of input
        //compre input to previous level
        if
         ( Math.fabs(xn) > level )
        {
            attack.tick(xn);
            attack.envelope => float level;
        }
        //feed input sample into envelope follower
        else
        {
            release.tick(xn);
            release.envelope => level;
        }
        
        //apply gain (if any) to input
        1 => float g; //how much compression to apply
        
        //HOW TO CALCULATE GAIN REDUCTION?
        if ( level > threshold )
            Math.pow(level/threshold, (1/ratio)-1) => g;
        
        return g*xn;
        
    }
    
    fun void attackTime (float t) 
    {
        second/samp => float srate;
        t*srate => float N;
        -Math.pow(Math.E, -1/N) => attack.a1;
        1-Math.fabs(attack.a1) => attack.b0;
    }
    
    fun void releaseTime (float t) 
    {
        second/samp => float srate;
        t*srate => float N;
        -Math.pow(Math.E, -1/N) => release.a1;
        1-Math.fabs(attack.a1) => release.b0;
    }
}

/*
SndBuf buf => EnvelopeFollower follower => blackhole;
buf => Compressor comp => dac;
db2lin(-60) => comp.threshold;
1000000 => comp.ratio;
0.0001 => comp.attackTime;
0.9 => comp.releaseTime;


1.0/comp.threshold => comp.gain;
me.dir()+"/CRASH.wav" => buf.read;
while(Math.random2f(0.001, 0.8)::second => now) { 0 => buf.pos;}
*/