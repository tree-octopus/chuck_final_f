public class qDrums {
    
    // duration of the current beat in the phrase
    float currBeat;
    // melodic phrase with durations
    float phrase[];
    // current index
    int curr;
    
    // how big the ticks, are in fraction of a beat
    int subdiv;
    
    // smallest subdivision
    dur tick;
    // measure
    dur meas;
    // turn the subdivisions into rhythms
    int counter;
    // current tick, next tick, in seconds
    float currTk, nextTk;
     // Time Signature Numerator, Time Signature Denominator
    float tsN, tsD;
    
    // beats per minute
    float BPM;    
    // durations in seconds!!
    dur beat;
    // beat duration in seconds
    float bS;
    SndBuf sb;
    //Bitcrusher bc;
    Gain g;
    
    time later;
    
    StkInstrument stk;
    SawOsc osky;
    Chubwrap chub;
    Chugewrap chuge;
    
    float midi[];
    0.5 => float defGain;
    
    ADSR env;
    LPF lpf; 
    HPF hpf;
    
    // initialize soundBuffer with a file
    fun void init(string file) {
        //sb => bc => env => lpf => hpf => g => dac;
        sb => env => lpf => hpf => g => dac;
        file => sb.read;
        sb.samples() => sb.pos;
        //0 => snare.pos        
    }
    
    fun void init() {
        //osky => bc => env => lpf => hpf => g => dac;
        osky => env => lpf => hpf => g => dac;
        // osky => dac;
        (.001::second , 0.2::second, 0.1, 0.05::second) => env.set;
        //defGain => g.gain;
    }
    
    fun void init(Chubwrap chubby)
    {
        chubby @=> chub;
        chub => dac;
    }
    
        
    fun void init(Chugewrap chugey)
    {
        chugey @=> chuge;
        chuge => dac;
    }
    
    fun void init(StkInstrument inst) {
        inst @=> stk;
        stk => env => lpf => hpf => g => dac;
        //stk => bc => env => lpf => hpf => g => dac;
        //(.001::second , 0.2::second, 0.1, 0.05::second) => env.set;
        defGain => g.gain;
    }
    
    // set the time signature, bpm, and subdivisions of beat
    // bpm - BPM
    // N - time signature numerator (beats per measure)
    // noteBeats - value of the beats: 1 for quarter note, 0.5 for eigth note
    // sd - subdivision; what fraction of a beat do we want to increment by.
    fun void setTimeSig(float bpm, float N, float noteBeats, int sd) {
        bpm => BPM;
        N => tsN;
        sd => subdiv;
        (60/BPM * noteBeats)::second => beat;
        beat * tsN   => meas;
        // increment by tiny subdivisions of beat
        beat/subdiv => tick;                      
    }

    // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times
    fun void play(float p[], dur theTime, float Rept) {
        now + Rept * theTime - tick => time tot;
        p @=> phrase;
        
        
        //<<< "time to play">>>;
        1 => int times;
        while(now < tot && times <= Rept) {
            now + theTime - 1::ms => later;
            0 => currBeat;
            0 => counter;
            0 => curr;
            if(now < tot) {
               while(now < later) {
                    // the current rhythmic note
                    phrase[curr] => currBeat;
                    // how much time has passed by current subdivision
                    counter*(tick/second) => currTk;
                    // how much time will have passed by next subdivision
                    (counter+1)*(tick/second) => nextTk;
                    
                    // to account for quantization errors, play something if it falls between
                    //   this subdivision and the next
                    if(currBeat >= currTk && currBeat < nextTk) {
                        1 => env.keyOn;
                        0 => sb.pos;
                        curr++;
                        if(curr >= phrase.size())
                            curr % phrase.size() => curr;
                    }                    
                    tick => now;
                    counter++;
                }    
            }    
            times++;  
        }
         
    }
    
     // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times
    fun void playChub(float p[], dur theTime, float Rept) {
        now + Rept * theTime - tick => time tot;
        p @=> phrase;
        
        
        //<<< "time to play">>>;
        1 => int times;
        while(now < tot && times <= Rept) {
            now + theTime - 1::ms => later;
            0 => currBeat;
            0 => counter;
            0 => curr;
            if(now < tot) {
               while(now < later) {
                    // the current rhythmic note
                    phrase[curr] => currBeat;
                    // how much time has passed by current subdivision
                    counter*(tick/second) => currTk;
                    // how much time will have passed by next subdivision
                    (counter+1)*(tick/second) => nextTk;
                    
                    // to account for quantization errors, play something if it falls between
                    //   this subdivision and the next
                    if(currBeat >= currTk && currBeat < nextTk) {
                        1 => env.keyOn;
                        1 => chub.noteOn;
                        curr++;
                        if(curr >= phrase.size())
                            curr % phrase.size() => curr;
                    }                    
                    tick => now;
                    counter++;
                }    
            }    
            times++;  
        }
         
    }
    
     // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times
    fun void playChuge(float p[], dur theTime, float Rept) {
        now + Rept * theTime - tick => time tot;
        p @=> phrase;
        
        
        //<<< "time to play">>>;
        1 => int times;
        while(now < tot && times <= Rept) {
            now + theTime - 1::ms => later;
            0 => currBeat;
            0 => counter;
            0 => curr;
            if(now < tot) {
               while(now < later) {
                    // the current rhythmic note
                    phrase[curr] => currBeat;
                    // how much time has passed by current subdivision
                    counter*(tick/second) => currTk;
                    // how much time will have passed by next subdivision
                    (counter+1)*(tick/second) => nextTk;
                    
                    // to account for quantization errors, play something if it falls between
                    //   this subdivision and the next
                    if(currBeat >= currTk && currBeat < nextTk) {
                        1 => env.keyOn;
                        1 => chuge.noteOn;
                        curr++;
                        if(curr >= phrase.size())
                            curr % phrase.size() => curr;
                    }                    
                    tick => now;
                    counter++;
                }    
            }    
            times++;  
        }
         
    }
    
  // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times, 
    //   with midi 'midi'
    fun void playStk(float p[], dur theTime, float Rept, float midi[]) {
        now + Rept * theTime - tick => time tot;
        p @=> phrase;       

        1 => int times;
        beat/second * 1/8 => float NoteOn;
        float prevBeat;
        time prevBeatTime;
        while(now < tot && times <= Rept) {
            now + theTime - 1::ms => later;
            0 => currBeat;
            0 => counter;
            0 => curr;
            
            while(now < later) {
                // the current rhythmic note
                phrase[curr] => currBeat;
                // how much time has passed by current subdivision
                counter*(tick/second) => currTk;
                // how much time will have passed by next subdivision
                (counter+1)*(tick/second) => nextTk;
                //<<< "to be played: ", currBeat >>>;
                //<<< "time: ", currTk >>>;
                // to account for quantization errors, play something if it falls between
                //   this subdivision and the next
                if(currBeat >= currTk && currBeat < nextTk) {
                    midi[curr] => Std.mtof => stk.freq;
                    //<<< midi[curr] >>>;
                    curr++;
                    1 => stk.noteOn;
                    1 => env.keyOn;  
                    if(curr >= phrase.size())
                        curr % phrase.size() => curr;
                    //currBeat => prevBeat;
                    now + NoteOn::second => prevBeatTime;
                } 
                if(now < prevBeatTime) {
                                     
                } else {
                }               
                tick => now;
                counter++;
            }
            times++;      
        }
    
    }
    
     // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times, 
    //   with midi 'midi'
    fun void playStk(float noteOns[], float noteOffs[], dur theTime, float Rept, float midi[]) {
        spork ~ this.notesOff(noteOffs, theTime, Rept, midi);
        now + Rept * theTime - tick => time tot;
        noteOns @=> phrase;       
        
        1 => int times;
        beat/second * 1/8 => float NoteOn;
        float prevBeat;
        time prevBeatTime;
        while(now < tot && times <= Rept) {
            now + theTime - 1::ms => later;
            0 => currBeat;
            0 => counter;
            0 => curr;
            
            while(now < later) {
                // the current rhythmic note
                phrase[curr] => currBeat;
                // how much time has passed by current subdivision
                counter*(tick/second) => currTk;
                // how much time will have passed by next subdivision
                (counter+1)*(tick/second) => nextTk;

                // to account for quantization errors, play something if it falls between
                //   this subdivision and the next
                if(currBeat >= currTk && currBeat < nextTk) {
                    midi[curr] => Std.mtof => stk.freq;                    
                    curr++;
                    1 => stk.noteOn;
                    1 => env.keyOn; 
                    if(curr >= phrase.size())
                        curr % phrase.size() => curr;
                    //currBeat => prevBeat;
                    now + NoteOn::second => prevBeatTime;
                } 
                if(now < prevBeatTime) {
                                       
                } else {
                }               
                tick => now;
                counter++;
            }
            times++;      
        }
    }

     // turns notes off
    fun void notesOff(float p[], dur theTime, float Rept, float midi[]) {
        //now + theTime - 1::ms => later;
        now + Rept * theTime - tick => time tot;
        p @=> phrase;       
        
        1 => int times;
        beat/second * 1/8 => float NoteOn;
        float prevBeat;
        time prevBeatTime;
        while(now < tot && times <= Rept) {
            now + theTime - 1::ms => later;
            0 => currBeat;
            0 => counter;
            0 => curr;
            
            while(now < later) {
                //<<< "inner" >>>;
                // the current rhythmic note
                phrase[curr] => currBeat;
                // how much time has passed by current subdivision
                counter*(tick/second) => currTk;
                // how much time will have passed by next subdivision
                (counter+1)*(tick/second) => nextTk;
                
                // to account for quantization errors, play something if it falls between
                //   this subdivision and the next
                if(currBeat >= currTk && currBeat < nextTk) {                  
                    curr++;
                    1 => stk.noteOff;
                    1 => env.keyOff; 
                    if(curr >= phrase.size())
                        curr % phrase.size() => curr;
                    now + NoteOn::second => prevBeatTime;
                } 
                if(now < prevBeatTime) {

                } else {
                }               
                tick => now;
                counter++;
            }
            times++;      
        }
    }                      
        // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times, 
    //   with midi 'midi'
    fun void playOsc(float p[],dur theTime, float Rept, float midi[]) {
        now + Rept * theTime - tick => time tot;
        p @=> phrase;       
        //<<< "playing stuff" >>>;
        1 => int times;
        float prevBeat;
        while(now < tot && times <= Rept) {
            now + theTime - 1::ms => later;
            0 => currBeat;
            0 => counter;
            0 => curr;
            
            while(now < later) {
                //<<< "inner" >>>;
                // the current rhythmic note
                phrase[curr] => currBeat;
                // how much time has passed by current subdivision
                counter*(tick/second) => currTk;
                // how much time will have passed by next subdivision
                (counter+1)*(tick/second) => nextTk;
                
                // to account for quantization errors, play something if it falls between
                //   this subdivision and the next
                if(currBeat >= currTk && currBeat < nextTk) {
                    midi[curr] => Std.mtof => osky.freq;     
                    //<<< Std.mtof(midi[curr]) >>>;
                    curr++;
                    1 => env.keyOn;
                    if(curr >= phrase.size())
                        curr % phrase.size() => curr;
                }               
                tick => now;
                counter++;
            }
            times++;      
        }
    }
    // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times, 
    //   with midi 'midi'
    fun void play(float p[], float noteOffs[],dur theTime, float Rept, float midi[]) {
        //now + theTime - 1::ms => later;
        spork ~ this.notesOffM(noteOffs, theTime, Rept);
        now + Rept * theTime - tick => time tot;
        p @=> phrase;       

        1 => int times;

        while(now < tot && times <= Rept) {
            now + theTime - 1::ms => later;
            0 => currBeat;
            0 => counter;
            0 => curr;
            
            while(now < later) {
                //<<< "inner" >>>;
                // the current rhythmic note
                phrase[curr] => currBeat;
                // how much time has passed by current subdivision
                counter*(tick/second) => currTk;
                // how much time will have passed by next subdivision
                (counter+1)*(tick/second) => nextTk;
                //<<< "to be played: ", currBeat >>>;
                //<<< "time: ", currTk >>>;
                // to account for quantization errors, play something if it falls between
                //   this subdivision and the next
                if(currBeat >= currTk && currBeat < nextTk) {
                    midi[curr] => Std.mtof => osky.freq;                    
                    curr++;
                    //<<< "note: ", midi[curr], Std.mtof(midi[curr]) >>>;
                    1 => env.keyOn;
                    if(curr >= phrase.size())
                        curr % phrase.size() => curr;
                    //currBeat => prevBeat;
                }              
                tick => now;
                counter++;
            }
            times++;      
        }
    }
    
    // turns notes off for oscillators
    fun void notesOffM(float p[], dur theTime, float Rept) {
        //now + theTime - 1::ms => later;
        now + Rept * theTime - tick => time tot;
        p @=> phrase;       
        
        1 => int times;
        beat/second * 1/8 => float NoteOn;
        float prevBeat;
        time prevBeatTime;
        while(now < tot && times <= Rept) {
            now + theTime - 1::ms => later;
            0 => currBeat;
            0 => counter;
            0 => curr;
            
            while(now < later) {
                //<<< "inner" >>>;
                // the current rhythmic note
                phrase[curr] => currBeat;
                // how much time has passed by current subdivision
                counter*(tick/second) => currTk;
                // how much time will have passed by next subdivision
                (counter+1)*(tick/second) => nextTk;
                
                // to account for quantization errors, play something if it falls between
                //   this subdivision and the next
                if(currBeat >= currTk && currBeat < nextTk) {                  
                    curr++;
                    1 => env.keyOff; 
                    if(curr >= phrase.size())
                        curr % phrase.size() => curr;
                    now + NoteOn::second => prevBeatTime;
                } 
                if(now < prevBeatTime) {

                } else {
                }               
                tick => now;
                counter++;
            }
            times++;      
        }
    }             
    
    // Takes a give phrase and swings it
    // midiPhrase[] - phrase to be swung (note Timings)
    // beat - what subdivision of the beat you want to be swung (16ths, 8ths)
    // swing - how hard to swing it!!
    fun static float[] swingIt(float midiPhrase[], dur beat, float swing) {
        float newPhrase[midiPhrase.size()];
        for(0 => int i; i < midiPhrase.size(); i++) {
            //<<< midiPhrase[i] >>>;
            midiPhrase[i] - 1 => newPhrase[i];
            //<<< "swung ? ", newPhrase[i] % 2 >>>;
            ((newPhrase[i] % 2) * (swing - 0.5) * 2 + newPhrase[i]) * beat/second => newPhrase[i];        
        }
        return newPhrase;
    }
    fun static float[] rtC(float chord[], float root) {
        chord @=> float temp[];
        for(0 => int i; i < temp.size(); i++) {
            temp[i] + root => temp[i];
        }
        return temp;    
    }
    fun static float[] noteLengths(float notes[], float durs[]) {
        float temp[notes.size()];
        for(0 => int i; i < temp.size(); i++) {
            notes[i] + durs[i] => temp[i];
        }
        return temp;  
    }
}