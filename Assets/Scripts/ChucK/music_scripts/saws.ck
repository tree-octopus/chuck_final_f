// final proj - Jeff Huang
// saw chords

int wait;
// print out all arguments
if(me.args() > 0)
{
    // how many measures to wait before starting to play!
    me.arg(0) => Std.atoi => wait;
}
// relevant private classes
// ================================================== //
// multi voice oscillator with envelope
class multiOscs extends Chubgraph
{
    // array of oscillators
    Osc oscs[];
    
    // envelopes for each oscillator in the array
    ExpEnv envs[];    
    
    // initialize with number of voices, and type of oscillator
    fun void init(int num, int type)
    {        
        Osc temp[num] @=> oscs;        
        ExpEnv tempEnv[num] @=> envs;
        
        // connect everything
        for(0 => int i; i < num; i++)
        {
            if(type == 3)
                SawOsc w @=> oscs[i];
            else if(type == 2)
                SqrOsc q @=> oscs[i];
            else if(type == 1)
                TriOsc t @=> oscs[i];
            else
                SinOsc s @=> oscs[i];
            0.9/num => oscs[i].gain;
            oscs[i] => envs[i] => outlet;
            (3::ms, 600::ms, 1, 50::ms) => envs[i].set;
        }
        
        1 => noteOff;
    }    
    
    fun void setEnv(dur a, dur d, float s, dur r)
    {
        for(0 => int i; i < envs.cap(); i++)
        {
            (a, d, s, r) => envs[i].set;
        }
    }
    
    // send note information to set frequencies
    fun void setNotes(float notes[])
    {
        for(0 => int i; i < oscs.cap(); i++)
        {
            notes[i] => Std.mtof => oscs[i].freq;
        }
    }
    
    fun void setGain(float theGain)
    {
        for(0 => int i; i < oscs.cap(); i++)
        {
            theGain => oscs[i].gain;
        }
    }
    
    // turn on envelope
    fun void noteOn(int no)
    {
        for(0 => int i; i < envs.cap(); i++)
        {
            no => envs[i].setKeyOn;
        }
    }
    
    // turn off envelope
    fun void noteOff(int no)
    {
        for(0 => int i; i < envs.cap(); i++)
        {
            no => envs[i].setKeyOff;
        }
    }
}

// lil FM thing
class miniFM extends Chubgraph
{
    multiOscs car;
    multiOscs mod; 
 
    float ratio;
    //float gain;
    
    fun void init(int voices, int car_type, int mod_type, float theRatio, float theGain)
    {
        car.init(voices, car_type);
        mod.init(voices, mod_type);
        theRatio => ratio;
        for(0 => int i; i < voices; i++)
        {
            mod.oscs[i] => car.oscs[i] => outlet;
            2 => car.oscs[i].sync;
            car.oscs[i].freq() * ratio => mod.oscs[i].freq; 
        }
        modGain(theGain);
        
        1 => noteOff;
    }
    
    fun void modGain(float theGain)
    {
        mod.setGain(theGain);
    }
    
    fun void carGain(float theGain)
    {
        car.setGain(theGain);
    }
    
    fun void setNotes(float notes[])
    {
        for(0 => int i; i < car.oscs.cap(); i++)
        {
            notes[i] => Std.mtof => car.oscs[i].freq;
            car.oscs[i].freq() * ratio => mod.oscs[i].freq;
        }
    }
    
    fun void noteOn(int no)
    {
        no => car.noteOn;
        no => mod.noteOn;
    }
    
    fun void noteOff(int no)
    {
        no => car.noteOff;
        no => mod.noteOff;
    }
    
}

// initialize - starting BPM and stuff
// ================================================== //
79 => float BPM;
// durations in musical time
(60./BPM)::second => dur qt;
qt*4. => dur meas;
qt/2. => dur et;
time later;

// chord notes and stuff
38. + 12 => float D3;
40. + 12 => float E3;
33. + 12 => float A2;
35. + 12 => float B2;

[0., 10., 2+12., 5+12., 9+12.] @=> float sus[];
[123123., 10., 2+12., 5+12., 9+12.] @=> float fake_sus[];
[0., 7.] @=> float bottom[];

quant.rtC([0., 10., 3+12., 5+12., 10+12.], D3) @=> float Dm7_1[];
quant.rtC([0.,  5.,   10., 3+12.,  7+12.], E3) @=> float Em7_1[];
quant.rtC([0., 10., 4+12., 9+12.,  1+24.], A2) @=> float A7alt_1[];
quant.rtC([0.,  7., 2+12., 6+12., 10+12.], B2) @=> float FshmM7_B[];

// initialize instruments
// ================================================== //
quant dummy;
//multiOscs bub => dac;
miniFM bub => LPF lpf => HPF hpf => dac;
9060 => lpf.freq;
190 => hpf.freq;
bub.init(5, 1, 3, 1, 100.);
bub.car.setEnv(3::ms, 600::ms, 1., 50.::ms);
bub.mod.setEnv(3::ms, 1100::ms, 0., 50.::ms);

miniFM bub2 => lpf => dac;
bub2.init(2, 1, 3, 1, 100.);
bub2.car.setEnv(3::ms, 600::ms, 1., 50.::ms);
bub2.mod.setEnv(3::ms, 1100::ms, 0., 50.::ms);

0.=> bub2.gain;
//bub.init(5, 1);
0.15 => bub.gain;
//bub.setNotes(Dm7_1);

// Sending OSC for visuals
//----------------------//
40000 => int outPort;

OscWrapper saw_out;
saw_out.send.setHost("localhost", outPort);
saw_out.setTimeSig(79, 4, 1, 1000);
"/LightColor" => string saw_add;
saw_add => saw_out.msg;

"/LightStop" => string saw_stop;

//----------------------//

// set stuff
// ================================================== //
dummy.setTimeSig(BPM, 4, 1, 1000);


// play stuff
// ================================================== //

0 => bub.gain;
1 => bub.noteOff;
now + wait*dummy.meas - 1::samp => later;
while(now < later)
{
    dummy.meas => now;
}
0.09 => bub.gain;
now + 8*dummy.meas - 1::samp => later;
int count;
int msr;
spork ~ saw_out.sendRhythm([0. * saw_out.beat/second], 1. * saw_out.meas, 8.);
while(now < later) 
{         
    if(msr % 4 == 0 && count % 4 == 0)
    {
        bub.setNotes(Dm7_1);
        1 => bub.noteOff;
    }        
    else if(msr % 4 == 1 && count % 4 == 0)
    {
        bub.setNotes(Em7_1);
        1 => bub.noteOff;
    }
    else if(msr % 4 == 2 && count % 4 == 0)
    {
        bub.setNotes(A7alt_1);
        1 => bub.noteOff;
    }
    else if(msr % 4 == 3 && count % 4 == 0)
    {
        bub.setNotes(FshmM7_B);
        1 => bub.noteOff;
    }   
    
    if(count % 4 == 0)
    {
        1 => bub.noteOn;
    }
    else if(count % 4 == 3)
    {
        //1 => bub.noteOff;
        msr++;  
    }
    count++;    
    dummy.beat => now;
}

bub =< dac;
bub => ExpEnv autopan => dac;
autopan.set(0.99 * dummy.beat, 0.01 * dummy.beat, 0., 50.::ms);
spork ~ saw_out.sendRhythm([0. * saw_out.beat/second], 1. * saw_out.beat, 4 * 20.);
now + 20*dummy.meas - 1::samp => later;
while(now < later) 
{         
    if(msr % 4 == 0 && count % 4 == 0)
    {
        bub.setNotes(Dm7_1);
        1 => bub.noteOff;
    }        
    else if(msr % 4 == 1 && count % 4 == 0)
    {
        bub.setNotes(Em7_1);
        1 => bub.noteOff;
    }
    else if(msr % 4 == 2 && count % 4 == 0)
    {
        bub.setNotes(A7alt_1);
        1 => bub.noteOff;
    }
    else if(msr % 4 == 3 && count % 4 == 0)
    {
        bub.setNotes(FshmM7_B);
        1 => bub.noteOff;
    }   
    
    if(count % 4 == 0)
    {
        1 => bub.noteOn;
    }
    else if(count % 4 == 3)
    {
        //1 => bub.noteOff;
        msr++;  
    }
    count++;    
    1 => autopan.setKeyOn;
    dummy.beat => now;
}

bub =< autopan;
bub => dac;
spork ~ saw_out.sendRhythm([0. * saw_out.beat/second], 1. * saw_out.meas, 1.);
now + 1*dummy.meas - 1::samp => later;
while(now < later) 
{         
    bub.setNotes(quant.rtC(sus, D3-2));
    1 => bub.noteOn;
    dummy.meas => now;
} 

1 =>bub.noteOff;

spork ~ saw_out.sendRhythm([0. * saw_out.beat/second], 1. * saw_out.meas, 1.);
now + 1*dummy.meas - 1::samp => later;
while(now < later) 
{         
    bub.setNotes(quant.rtC(sus, D3));
    1 => bub.noteOn;
    dummy.meas => now;
}  

1 =>bub.noteOff;

0.1 => bub2.gain;

spork ~ saw_out.sendRhythm([0. * saw_out.beat/second], 1. * saw_out.meas, 1.);
now + 1.*dummy.meas - 1::samp => later;
while(now < later) 
{         
    bub.setNotes(quant.rtC(fake_sus, D3));
    bub2.setNotes(quant.rtC(bottom, D3-11));
    1 => bub2.noteOn;
    1 => bub.noteOn;
    1.*dummy.meas => now;
}  

saw_stop => saw_out.msg;
spork ~ saw_out.sendRhythm([0. * saw_out.beat/second], .2 * saw_out.meas, 1.);
1 =>bub.noteOff;
1 =>bub2.noteOff;
<<< "why no turn off" >>>;
0.2 * dummy.meas => now;
