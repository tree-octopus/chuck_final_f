// final proj - Jeff Huang
//
// Score
// ================================================== //

//"Scripts/ChucK/music_scripts/" => string folders;
"/music_scripts/" => string folders;
// effects classes
me.dir(-1) + folders + "resonator.ck" => string resonPath;
me.dir(-1) + folders + "feedback.ck" => string feedPath;
me.dir(-1) + folders + "exp_env.ck" => string expPath;
Machine.add(resonPath) => int resonID;
Machine.add(feedPath) => int feedID;
Machine.add(expPath) => int expID;
chout <= expPath <= IO.newline();
// wrapper classes
me.dir(-1) + folders + "chubwrap.ck" => string chubPath;
me.dir(-1) + folders + "chugewrap.ck" => string chugePath;
Machine.add(chubPath) => int chubID;
Machine.add(chugePath) => int chugeID;

// sequencer class
me.dir(-1) + folders + "quant.ck" => string quantPath;
me.dir(-1) + folders + "qDrums.ck" => string qDrumsPath;
me.dir(-1) + folders + "oscwrapper.ck" => string oscPath;
Machine.add(quantPath) => int quantID;
Machine.add(qDrumsPath) => int qDrumsID;
Machine.add(oscPath) => int oscID;

// synthesis instruments
me.dir(-1) + folders + "snare2.ck" => string snare2Path;
me.dir(-1) + folders + "qymbal.ck" => string qymbalPath;
me.dir(-1) + folders + "nymbal.ck" => string nymbalPath;
me.dir(-1) + folders + "kick909_chugen.ck" => string kick909Path;
Machine.add(snare2Path) => int snare2ID;
Machine.add(qymbalPath) => int nymbalID;
Machine.add(nymbalPath) => int qymbalID;
Machine.add(kick909Path) => int kick909ID;

// sub-score files
me.dir(-1) + folders + "saws.ck:14" => string sawsPath;
me.dir(-1) + folders + "perc.ck:1" => string percPath;
me.dir(-1) + folders + "bass.ck:10" => string bassPath;
me.dir(-1) + folders + "aurora.ck:39" => string auroraPath;

Machine.add(sawsPath) => int sawsID;
Machine.add(percPath) => int percID;
Machine.add(bassPath) => int bassID;
Machine.add(auroraPath) => int auroraID;

