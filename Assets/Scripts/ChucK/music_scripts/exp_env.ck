public class ExpEnv extends Chugen
{       
    // Envelope states
    static int ATTACK, DECAY, SUSTAIN, RELEASE, DONE;
    0 => ATTACK;
    1 => DECAY;
    2 => SUSTAIN;
    3 => RELEASE;
    4 => DONE;
    DONE => int state;
    
    // Envelope times, in samples!!
    float att_time, dec_time, sus_level, rel_time;    
    time start, att_later, dec_later, rel_later;
    
    // how many samples the envelope has been active for
    float samps;
    
    0 => int keyOn;
    1 - keyOn => int keyOff;
    
    0 => int wasOn;
    
    // parameters for calculating exponential stuff
    4 => static int k;
    static float grow, x_norm, a, b, c;
    
    // number to multiply input sample by
    float thingy;
        
    // set Envelope parameters!!!!
    fun void set(dur a, dur d, float s, dur r)
    {
        a/samp => att_time;
        d/samp => dec_time;
        Math.max(Math.min(s,1),0) => sus_level;
        r/samp => rel_time;
    }    
    
    fun void setKeyOn(int ko)
    {
        Math.max(Math.sgn(ko), 0) $ int => keyOn;
        //<<< "keyOn:", keyOn >>>;
        1 - keyOn => keyOff;
        
        if(keyOn == 1)
        {
            //<<< "keyOnned" >>>;
            now/samp => samps;
            now => start;
            start + att_time::samp => att_later;
            att_later + dec_time::samp => dec_later;
            1 => wasOn;
        }
        
    }

    fun void setKeyOff(int ko)
    {
        Math.max(Math.sgn(ko), 0) $ int => keyOff;
        1 - keyOff => keyOn;
        if(keyOff == 1)
        {
            //<<< "keyOffed" >>>;
            now/samp => samps;
            now => start;
            start + rel_time::samp => rel_later; 
        }        
    }
    
    fun int getState()
    {
        return state;
    }
    
    fun float tick ( float xn )
    {
        // envelope on
        if(keyOn >= 1)
        {
            // attacking!!
            if(samps < att_later/samp)
            {
                ATTACK => state;
                // take the current sample, find where on the curve it is...                
                // * curve y starts at 0, goes to 1
                // * curve x starts at start, goes to att_later
                //<<< "now:", (samps)$float >>>;
                expStretch(samps, -1, -1, att_later/samp, start/samp, 1., 0.) => thingy;
            }
            // decaying!!
            else if(samps < dec_later/samp && samps >= att_later/samp)
            {
                DECAY => state;
                // take the current sample, find where on the curve it is...                
                // * curve y starts at 1, goes to sus_level
                // * curve x starts at att_later, goes to dec_later
                1 +=> samps;
                expStretch(samps, -1, 1, dec_later/samp, att_later/samp, 1., sus_level) => thingy;
            }
            // sustaining!!
            else if(samps >= dec_later/samp)
            {
                SUSTAIN => state;
                // holds this till keyOff signal!!
                sus_level => thingy;
            }
            
            // increment samples
            1 +=> samps;
        }
        // envelope off
        else if(keyOn <= 0)
        {
            if(wasOn == 1)
            {                
                // release!              
                if(samps < rel_later/samp)
                {                   
                   RELEASE => state; 
                    //<<< "turning off!">>>;
                    // take the current sample, find where on the curve it is...                
                    // * curve y starts at sus_level, goes to 0
                    // * curve x starts at new start, goes to rel_later
                    expStretch(samps, -1, 1, rel_later/samp, start/samp, sus_level, 0) => thingy;
                    1 +=> samps;
                }
                // you're done!
                else
                {
                    DONE => state;
                    0 => wasOn;
                }                
            }
            else
            {
                DONE => state;
            }
        }  
        // return the signal multiplied by the envelope at the current time
        return thingy * xn;        
    }   

    // return the value of the envelope!! 
    fun float value()
    {
        return thingy;
    }
    
    // produces a piece of the exponential function stretched so it actually touches 0
    // * t - time variable, how far along you are in the curve
    // * x_sign - sign of the x variable (e^x vs e^-x)
    // * y_sign - sign of the exponential (e^x vs -e^x)
    // * x_max - maximum x value we want to fit the graph to
    // * x_min - minimum x value '' ....
    // * y_max - maximum y value '' ....
    // * y_min - minimum y value '' ....
    fun static float expStretch(float t, float x_sign, float y_sign, 
        float x_max, float x_min, float y_max, float y_min)
    {
        if(x_sign >= 0)
            1. => x_sign;
        else
            -1. => x_sign;
        if(y_sign >= 0)
            1. => y_sign;
        else
            -1. => y_sign;
        // to account for counting +1
        -1 +=> x_max;
        
        Math.pow(Math.E, x_sign * k) => grow;
        (t - x_min) / (x_max - x_min) => x_norm;
        (y_max - y_min) / (1 - grow) => a;
        y_min + a => b;        
        
        if((y_sign >= 0 && x_sign >= 0) || (y_sign < 0 && x_sign < 0))
        {
            return b - a * Math.pow(grow, x_norm);
        }            
        else if((x_sign >= 0 && y_sign < 0) || (x_sign < 0 && y_sign >= 0))
        {
            return y_max - (b - a * Math.pow(grow, x_norm)) + y_min;
        }   
    }       
}
