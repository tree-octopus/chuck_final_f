BlowHole hole => Gain g1 => PitShift pit => HPF hpf => LPF lpf => BRF brf => ADSR env => Gain post => dac;


0.04 => pit.shift;
10 => g1.gain;
1. => hole.noteOn;
.8 => hole.vent;
0.44 => hole.pressure;
.7 => hole.reed;

200 => hole.freq;
10 => post.gain;

750 => brf.freq;
0.4 => brf.Q;

180 => hpf.freq;
1000 => lpf.freq;
0.4 => lpf.Q;
(500::ms, 50::ms, 1., 0::ms) => env.set;
    1 => env.keyOn;
while(true) {
    //Math.random2f
    //Math.random2f(30,40) => chor.modDepth;
    //Math.random2(20, 180)::ms => chor.baseDelay;
    Math.sin(2*pi*(now/second)*0.2 + 2) * 3 + 5 => g1.gain;
    Math.cos(2*pi*(now/second)*0.22) * 300 + 5000 => lpf.freq;
    5::ms=> now;
}
