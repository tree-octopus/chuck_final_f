public class Nymbal extends Chubwrap
{    
    ResonZ rsn[100];
    Noise n => ExpEnv env;
    BPF bpf => HPF hpf => outlet;
    12000 => bpf.freq;
    5000 => hpf.freq;
    1 => bpf.Q;
    100 => float rsnQ;
    
    Noise n2 => env => BPF bpf2 => outlet;
    8000 => bpf2.freq;
    
    (5::ms, 500::ms, 0, 0::ms) => env.set;
    150 => int seed;
    
    for(0 => int i; i < rsn.cap(); i++)
    {
        env => rsn[i] => bpf;
        Math.random2f(100, 123) => Std.mtof => rsn[i].freq;
        rsnQ => rsn[i].Q;
    }
    
    fun void calc()
    {
        for(0 => int i; i < rsn.cap(); i++)
        {
            env => rsn[i] => outlet;
            Math.random2f(1000, 10000) => rsn[i].freq;
            100 => rsn[i].Q;
        }        
    }
    
    fun void setQ(float newQ)
    {
        newQ => rsnQ;
        for(0 => int i; i < rsn.cap(); i++)
        {            
            rsnQ => rsn[i].Q;
        }  
    }
    fun void noteOn(int no)
    {
        no => env.setKeyOn;
    }
    
    fun void setSeeded(int seeder)
    {
        Math.srandom(seeder);
    }
}
/*
Nymbal ninny => dac;
0.5 => ninny.gain;
(30::ms, 1500::ms, 0, 0::ms) => ninny.env.set;
//0.2 => ninny.bpf.Q;
8000 => ninny.bpf.freq;
3000 => ninny.hpf.freq;
200 => ninny.setQ;
1 => ninny.noteOn;

5::second => now;
*/