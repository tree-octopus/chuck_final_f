int wait;
// print out all arguments
if(me.args() > 0)
{
    // how many measures to wait before starting to play!
    me.arg(0) => Std.atoi => wait;
}

// initialize - starting BPM and stuff
// ================================================== //
79 => float BPM;
// durations in musical time
(60./BPM)::second => dur qt;
qt*4. => dur meas;
qt/2. => dur et;
time later;

quant dummy;
dummy.setTimeSig(BPM, 4, 1, 1000);
now + wait*dummy.meas - 1::samp => later;
while(now < later)
{
    dummy.meas => now;
}

// Sending OSC for visuals
//----------------------//
40000 => int outPort;
OscWrapper aur_out;
aur_out.send.setHost("localhost", outPort);
aur_out.setTimeSig(79, 4, 1, 1000);
"/Displacement" => string aur_add;
"/DispOff" => string aur_off;
"/ITSOVER" => string OVER;
aur_add => aur_out.msg;
//----------------------//

SndBuf chunk => Gain chunkG => Resonator rsn => Gain gf => Feedback f => Dyno dyno => GVerb gv => Gain master => ExpEnv fader => dac;
me.dir(0) + "chunk_0.wav" => string path => chunk.read;

(0::ms, 600::ms, 1.0, 2*aur_out.meas) => fader.set;
0 => chunk.pos;
0.1 => chunkG.gain;
15 => master.gain;
0.05 => gf.gain;
0.6 => f.feedback.gain;

15::second => gv.revtime;
0.4 => gv.tail;
50 => gv.roomsize;

dyno.limit();
0.4 => dyno.thresh;
//f.delay.delay;
//5::second => now;
[0., 10.-0.32, 2+12., 5+12., 9+12.] @=> float sus[];

rsn.init();
rsn.set(quant.rtC(sus, 24+12.));

//<<<"samples: ", chunk.samples()>>>;
//spork ~ delayLFO();
1 => fader.setKeyOn;
spork ~ aur_out.sendRhythm([0. * aur_out.beat/second], 1 * aur_out.meas, 2.);
now + 2*dummy.meas - 1::samp => later;
int k;
while(now < later) {
    Math.random2f(-2,-0.5) => chunk.rate;
    if(chunk.rate() == 0.0)
        0.01 => chunk.rate;
    if(chunk.rate() < 0)
        chunk.samples() => chunk.pos;
    else if(chunk.rate() > 0)
        0 => chunk.pos;

        
    <<< chunk.rate() , "k">>>;
    (1/Math.fabs(chunk.rate()) * chunk.samples())::samp => now;
    0 => chunk.pos;
    
    k++;
}

spork ~ aur_out.sendRhythm([0. * aur_out.beat/second], 1 * aur_out.meas, 1.);
rsn.set(quant.rtC(sus, 26+12.));
now + 1*dummy.meas - 1::samp => later;
while(now < later) {
    Math.random2f(-2,-0.5) => chunk.rate;
    if(chunk.rate() == 0.0)
        0.01 => chunk.rate;
    if(chunk.rate() < 0)
        chunk.samples() => chunk.pos;
    else if(chunk.rate() > 0)
        0 => chunk.pos;

        
    <<< chunk.rate() , "k">>>;
    aur_out.meas => now;
    0 => chunk.pos;
    
    k++;
}
aur_off => aur_out.msg;
spork ~ aur_out.sendRhythm([0. * aur_out.beat/second], 1 * aur_out.meas, 1.);
now + 1*dummy.meas - 1::samp => later;
1 => fader.setKeyOff;
while(now < later) {
    Math.random2f(-2,-0.5) => chunk.rate;
    if(chunk.rate() == 0.0)
        0.01 => chunk.rate;
    if(chunk.rate() < 0)
        chunk.samples() => chunk.pos;
    else if(chunk.rate() > 0)
        0 => chunk.pos;

        
    <<< chunk.rate() , "k">>>;
    (1/Math.fabs(chunk.rate()) * chunk.samples())::samp => now;
    0 => chunk.pos;
    
    k++;
}

OVER => aur_out.msg;
spork ~ aur_out.sendRhythm([0. * aur_out.beat/second], 1 * aur_out.meas, 1.);
now + 1*dummy.meas - 1::samp => later;
1 => fader.setKeyOff;
while(now < later) {
    Math.random2f(-2,-0.5) => chunk.rate;
    if(chunk.rate() == 0.0)
        0.01 => chunk.rate;
    if(chunk.rate() < 0)
        chunk.samples() => chunk.pos;
    else if(chunk.rate() > 0)
        0 => chunk.pos;

        
    <<< chunk.rate() , "k">>>;
    (1/Math.fabs(chunk.rate()) * chunk.samples())::samp => now;
    0 => chunk.pos;
    
    k++;
}

<<< "fading" >>>;
1*dummy.meas - 1::samp => now;


2*aur_out.meas => now;
/*

aur_off => aur_out.msg;
spork ~ aur_out.sendRhythm([0. * aur_out.beat/second], 1 * aur_out.meas, 1.);
1 => fader.setKeyOff;
200::ms => now;
OVER => aur_out.msg;
spork ~ aur_out.sendRhythm([0. * aur_out.beat/second], 1 * aur_out.meas, 1.);

1900::ms => now;
*/

//600::ms => now;

/*OVER => aur_out.msg;
spork ~ aur_out.sendRhythm([0. * aur_out.beat/second], 1 * aur_out.meas, 1.);
2::ms => now;
*/

fun void delayLFO() {
    //SinOsc s => f.delay.delay;
    while(true) {
        //(Math.sin(2*pi*(now/second)*0.9) * 2.)*Math.randomf() - 0.2 => chunk.rate;
        //(Math.sin(2*pi*(now/second)*0.04) * 5. + 50)::ms => f.delay.delay;
        //(Math.sin(2*pi*(now/second)*0.4 + 2.) * 200. + 700)::ms => f.delay.delay;
        10::ms => now;
    }
}
