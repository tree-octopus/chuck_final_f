//<<< "added impreson ">>>;
// Impulse and resonator

// kind of modelling modal synthesis
public class ImpReson extends Chubwrap
{
    Impulse imp;
    ResonZ rz[];
    Gain g => outlet;
    // preset
    0 => int pre;
    40 => float imp_hardness;
    // modal frequency ratios, and resonances
    [[#(1.,4000), #(2.756,1000), #(5.423,2200) 
        ,#(8.988,400), #(13.448,800), #(18.680,1000)]] @=> complex presetArray[][];
    init(presetArray[0], 440);
    
    float root;   
    
    fun void init(complex ratios[], float b)
    {
        ResonZ temp[ratios.cap()] @=> rz;
        b => root;
        if(pre == 0)
        {
            for(0 => int i; i < rz.cap(); i++)
            {
                imp => rz[i] => g;
                ratios[i].re * root => rz[i].freq;
                ratios[i].im => rz[i].Q;
                //<<< rz[i].freq() >>>;
                
            }
        }
        else
        {
            // if no preset
            for(0 => int i; i < rz.cap(); i++)
            {
                imp => rz[i] => g;
                ratios[i].re * root => rz[i].freq;
                //<<< rz[i].freq() >>>;
                Math.fabs(10 - i) => rz[i].Q;
            }
        }
    }
    
    fun void preset(int thePreset)
    {
        thePreset => pre;
        init(presetArray[pre], 440);
    }
    
    fun void noteOn(int no)
    {
        if(pre == 0)
            no * imp_hardness => imp.next;
        else
            no => imp.next;
    }
    
    fun void freq(float f)
    {
        f => root;
        for(0 => int i; i < rz.cap(); i++)
        {
            imp => rz[i] => g;
            presetArray[pre][i].re * root => rz[i].freq;
            //<<< rz[i].freq() >>>;
        }
    }
    
    fun void strike(float strikePos, float vel)
    {
        if(pre == 0)
        {
            strikeHere(strikePos);
            vel * imp_hardness => imp.next;
        }
    }
    
    fun void strikeHere(float strikePos)
    {
        
    }
} 

/*
ImpReson ipr => dac;

(1., 1.) => ipr.strike;

5::second => now;
*/