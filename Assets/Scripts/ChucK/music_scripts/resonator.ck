// Author: Jeff Huang
// A resonator that uses delay lines and resonant filters to 
//   produce resonance at the specified pitches
public class Resonator extends Chubgraph 
{
    ResonZ rz[5];
    Delay d[5];
    Gain g[5];
    Gain f[5];
    //ADSR env;
    //(0.006::second, 0.5::second, 0, 0::second) => env.set;
    fun void init() {
        for(0 => int i; i < rz.size(); i++) {
            1.5/rz.size() => g[i].gain;
            2::second => d[i].max;
            inlet => d[i] => rz[i] => g[i] => outlet;
            d[i] => f[i] => d[i];
            0.96 => f[i].gain;
            if(i > 0)
                0 => rz[i].freq;
            0.4 => rz[i].Q;
        
        }
    }

    // provide resonator frequency in MIDI notes
    fun void set(float notes[], float Q[]) {
        for(0 => int i; i < Math.min(notes.size(), rz.size()); i++) {
            notes[i] => Std.mtof => rz[i].freq;
            (1/Std.mtof(notes[i]))::second => d[i].delay;
            Q[i] => rz[i].Q;
        }
    }
    
    // set the pitches of the resonator!!
    //   if the note array is smaller than resonator's array, 
    //   all non-set notes are set to 0 frequency (should be silent)
    fun void set(float notes[]) {
        //<<< " FCISK " >>>;
        for(0 => int i; i < rz.size(); i++) {
            if(i < notes.size()) {
                //<<< "why" >>>;
                notes[i] => Std.mtof => rz[i].freq;
                //<<< rz[i].freq() >>>;
                (1/Std.mtof(notes[i]))::second => d[i].delay;
            } else {
                //<<< "elsed" >>>;
                0 => rz[i].freq;
            }
            //<<< rz[i].freq() >>>;
            //0.4 => rz[i].Q;
        }
    }
    
    fun void setGain(float gainers)
    {
        for(0 => int i; i < g.cap(); i++)
        {
            gainers => g[i].gain;
        }
    }
}