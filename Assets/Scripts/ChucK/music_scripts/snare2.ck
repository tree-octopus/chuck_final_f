public class Snare2 extends Chubwrap
{
   SinOsc body;
   200 => float body_freq => body.freq;
   Noise n => BPF bpf;
    
   5000 => bpf.freq; 
   1 => bpf.Q;
     
   bpf => ExpEnv amp_env => outlet;
   body => amp_env;
   (2.2::ms, 160::ms, 0, 440::ms) => amp_env.set; 
    
   ExpEnv pitch_env => blackhole;
   (0::ms, 80::ms, 0, 0::ms) => pitch_env.set;
    
   5000 => bpf.freq;
    
    fun void noteOn(int no)
    {
        no => pitch_env.setKeyOn;
        no => amp_env.setKeyOn;

        while(now < amp_env.dec_later)
        {
            pitch_env.value() * 250 + body_freq => body.freq;
            1::samp => now;
        }
    }
    
    fun void noteOff(int no)
    {
        no => amp_env.setKeyOff;
    }   
}

/*
Snare2 sn => dac;

0.5 => sn.gain;
sn.body_freq => float old_freq;
for(0 => int i; i < 20; i++)
{
    3.54*(20-i) + old_freq => sn.body_freq;
    <<< sn.body_freq >>>;
    spork ~ sn.noteOn(1); 
    (60-i)::ms => now;
}


2::second => now;
*/