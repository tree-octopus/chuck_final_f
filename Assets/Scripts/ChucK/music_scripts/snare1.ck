class Snare1 extends Chugen
{
    // the base frequency
    260 =>  float freq;
    // phase junk
    pi/2. => float phase;
    // counter variable
    float p;
    // noise for the SH snare
    Noise n => BPF bpf;
    800 => bpf.freq;
    
    // pitch envelope
    ExpEnv pitch_env => blackhole;
    (0::ms, 50::ms, 0, 0::ms) => pitch_env.set;
    // amplitude envelope
    ExpEnv amp_env => blackhole;
    (2.2::ms, 6::ms, 0.5, 440::ms) => amp_env.set;
    // sustain for 60 ms
    60 => float sus_time;
    0 => int didSus;
    second/samp => float srate;
    
    float temp;
    float samp_old;
    
    fun float tick ( float xn )
    {    
        if((p >= amp_env.dec_later/samp + (sus_time::ms)/samp) && (didSus == 0))
        {
            1 => noteOff;
        }
        else
        {

        }        
        amp_env.value() * (Math.sin(2 * pi * 
                    ((p) * freq/srate + pitch_env.value() * -2) + phase) + 0.5 * Math.random2f(-1,1))=> temp;
        
        1 +=> p;

        return temp;
    }
    
    fun void noteOn(int no)
    {
        no => pitch_env.setKeyOn;
        no => amp_env.setKeyOn;
        now/samp => p;
        0 => didSus;
    }
    
    fun void noteOff(int no)
    {
        no => amp_env.setKeyOff;
        1 => didSus;
    }
}
/*
Snare1 sn => dac;

1 => sn.noteOn;

2::second => now;
*/