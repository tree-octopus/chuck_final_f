// final proj - Jeff Huang
// Percussion

int wait;
// print out all arguments
if(me.args() > 0)
{
    // how many measures to wait before starting to play!
    me.arg(0) => Std.atoi => wait;
}

// initialize - starting BPM and stuff
// ================================================== //
79 => float BPM;
// durations in musical time
(60./BPM)::second => dur qt;
qt*4. => dur meas;
qt/2. => dur et;
time later;

// initialize instruments
// ================================================== //
qDrums kick;
Kick909 kk;
35 => kk.freq;
0.4 => kk.gain;
kick.init(kk);

qDrums snare;
Snare2 sn;
0.28 => sn.gain;
//180 => sn.body_freq;
snare.init(sn);

qDrums shaker;
Shakers shak;
0 => shak.preset;
2.5 => shak.gain;
shaker.init(shak);
8000 => shaker.lpf.freq;
3000 => shaker.hpf.freq;

qDrums chh1;
Qymbal cq;
0.4 => cq.gain;
(2::ms, 150::ms, 0, 0::ms) => cq.env.set;
chh1.init(cq);

qDrums ohh1;
Qymbal ny;
0.4 => ny.gain;
(20::ms, 1500::ms, 0, 0::ms) => ny.env.set;
//8000 => ny.bpf.freq;
//3000 => ny.hpf.freq;
//200 => ny.setQ;
ohh1.init(ny);

// Sending OSC for visuals
//----------------------//
40000 => int outPort;
OscWrapper out;
out.send.setHost("localhost", outPort);
out.setTimeSig(79, 4, 1, 1000);
"/TexFadeIn" => string fade_in;
"/TexFadeOut" => string fade_out;
fade_in => out.msg;

OscWrapper kick_out;
kick_out.send.setHost("localhost", outPort);
kick_out.setTimeSig(79, 4, 1, 1000);
"/TexCamRot" => string kick_add;
kick_add => kick_out.msg;

OscWrapper snare_out;
snare_out.send.setHost("localhost", outPort);
snare_out.setTimeSig(79, 4, 1, 1000);
kick_add => snare_out.msg;

OscWrapper shake_out;
shake_out.send.setHost("localhost", outPort);
shake_out.setTimeSig(79, 4, 1, 1000);
"/MirrorRoll" => string shake_add;
shake_add => shake_out.msg;

OscWrapper hh_out;
hh_out.send.setHost("localhost", outPort);
hh_out.setTimeSig(79, 4, 1, 1000);
shake_add => hh_out.msg;

OscWrapper ohh_out;
ohh_out.send.setHost("localhost", outPort);
ohh_out.setTimeSig(79, 4, 1, 1000);
"/MirrorSymm" => string ohh_add;
ohh_add => ohh_out.msg;
//----------------------//

// set stuff
// ================================================== //
kick.setTimeSig(BPM, 4, 1, 1000);
snare.setTimeSig(BPM, 4, 1, 1000);
shaker.setTimeSig(BPM, 4, 1, 1000);
chh1.setTimeSig(BPM, 4, 1, 1000);
ohh1.setTimeSig(BPM, 4, 1, 1000);

[0. * kick.beat/second, 0.8 * kick.beat/second, 
    2. * kick.beat/second, 3.8 * kick.beat/second] @=> float kickPhraseA[];
[1.2 * snare.beat/second] @=> float snarePhraseA[];

[0. * kick.beat/second, 0.6 * kick.beat/second, 
    2. * kick.beat/second, 2.75 * kick.beat/second, 3.8 * kick.beat/second] @=> float kickPhraseB[];
[1. * snare.beat/second, 3. * snare.beat/second] @=> float snarePhraseB[];
[0.4 * shaker.beat/second] @=> float shakerPhrase[];
[0.4 * chh1.beat/second, 1.8 * chh1.beat/second, 2.4 * chh1.beat/second, 3.4 * chh1.beat/second] @=> float chh1Phrase[];
[3.8 * ohh1.beat/second] @=> float ohh1Phrase[];

// play stuff
// ================================================== //
now + wait*kick.meas => later;
spork ~ out.sendRhythm([(wait-0.5) * out.meas/second], wait * out.meas, 1.);
wait * kick.meas => now;
/*while(now < later) {
    kick.meas => now;
}*/

// A

spork ~ kick.playChuge(kickPhraseA, 1*kick.meas, 6);
spork ~ snare.playChub(snarePhraseA, 1*snare.meas, 6); 

spork ~ kick_out.sendRhythm(kickPhraseA, 1*kick.meas, 6);
spork ~ snare_out.sendRhythm(snarePhraseA, 1*snare.meas, 6);

6 * kick.meas => now;
/*now + 6*kick.meas => later;
while(now < later) {
    kick.meas => now;
}*/

// B
spork ~ kick.playChuge(kickPhraseB, 1*kick.meas, 31);
spork ~ snare.playChub(snarePhraseB, 1*snare.meas, 31);

spork ~ kick_out.sendRhythm(kickPhraseB, 1*kick.meas, 31);
spork ~ snare_out.sendRhythm(snarePhraseB, 1*snare.meas, 31);

spork ~ shaker.playStk(shakerPhrase, 1*shaker.beat, 4*31, [40.]);
spork ~ chh1.playChub(chh1Phrase, 1*chh1.meas, 31);
spork ~ ohh1.playChub(ohh1Phrase, 1*ohh1.meas, 31);

spork ~ shake_out.sendRhythm(shakerPhrase, 1*shaker.beat, 4*31);
spork ~ hh_out.sendRhythm(chh1Phrase, 1*chh1.beat, 31);
spork ~ ohh_out.sendRhythm(ohh1Phrase, 1*ohh1.meas, 31);

31 * kick.meas => now;
/*now + 31*kick.meas => later;
while(now < later) {
    kick.meas => now;
}

