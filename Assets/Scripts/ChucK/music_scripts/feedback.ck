// Author: Jeff Huang
// A simple Delay with a feedback
public class Feedback extends Chubgraph {
    inlet => Delay delay => outlet;
    delay => Gain feedback => delay;
    2::second => delay.max;
    50::ms => delay.delay;
}
    