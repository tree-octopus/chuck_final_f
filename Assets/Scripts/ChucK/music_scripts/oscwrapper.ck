public class OscWrapper extends qDrums
{    
    OscSend send;
    string msg;
    
    // sends a tick out when rhythmic phrase is played
    fun void sendRhythm(float p[],dur theTime, float Rept) {
        now + Rept * theTime - tick => time tot;
        p @=> phrase;       

        1 => int times;
        while(now < tot && times <= Rept) {
            now + theTime - 1::ms => later;
            0 => currBeat;
            0 => counter;
            0 => curr;
            
            while(now < later) {
                phrase[curr] => currBeat;
                counter*(tick/second) => currTk;
                (counter+1)*(tick/second) => nextTk;

                if(currBeat >= currTk && currBeat < nextTk) {                 
                    curr++;
                    <<< "sending" >>>;
                    send.startMsg(msg + ", f");
                    send.addFloat(1.);
                    if(curr >= phrase.size())
                        curr % phrase.size() => curr;                    
                }              
                tick => now;
                counter++;
            }
            times++;      
        }
    }
}

/*
40000 => int outPort;
OscWrapper out;
out.send.setHost("localhost", outPort);
out.setTimeSig(79, 4, 1, 1000);
"/bilbo" => out.msg;


OscWrapper out2;
out2.send.setHost("localhost", outPort);
out2.setTimeSig(79, 4, 1, 1000);
"/bilbo2" => out2.msg;

[0.5 * out.beat/second, 1.5 * out.beat/second, 2.5 * out.beat/second, 3.5 * out.beat/second] @=> float tester1[];
[0. * out2.beat/second, 1. * out2.beat/second, 2. * out2.beat/second, 3. * out2.beat/second] @=> float tester2[];
spork ~ out.sendRhythm(tester1, 1 * out.meas, 4.);
spork ~ out2.sendRhythm(tester2, 1 * out2.meas, 4.);
4. * out2.meas => now;
*/