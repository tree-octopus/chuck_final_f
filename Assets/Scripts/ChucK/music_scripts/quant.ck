// use this for sequencing!!
public class quant {
    
    // duration of the current beat in the phrase
    float currBeat;
    // melodic phrase with durations
    float phrase[];
    // current index
    int curr;
    
    // how big the ticks, are in fraction of a beat
    int subdiv;
    
    // smallest subdivision
    dur tick;
    // measure
    dur meas;
    // turn the subdivisions into rhythms
    int counter;
    // current tick, next tick, in seconds
    float currTk, nextTk;
     // Time Signature Numerator, Time Signature Denominator
    float tsN, tsD;
    
    // beats per minute
    float BPM;    
    // durations in seconds!!
    dur beat;
    // beat duration in seconds
    float bS;
    SndBuf sb;
    //Bitcrusher bc;
    Gain g;
    
    time later;
    
    StkInstrument stk;
    SawOsc osky;
    
    Chubwrap chub;
    Chugewrap chuge;
    
    float midi[];
    0.5 => float defGain;
    
    ADSR env;
    LPF lpf; 
    HPF hpf;
    
    // initialize soundBuffer with a file
    fun void init(string file) {
        //sb => bc => env => lpf => hpf => g => dac;
        sb => env => lpf => hpf => g => dac;
        file => sb.read;
        sb.samples() => sb.pos;
        //0 => snare.pos        
    }
    
    fun void init() {
        //osky => bc => env => lpf => hpf => g => dac;
        osky => env => lpf => hpf => g => dac;
        // osky => dac;
        (.001::second , 0.2::second, 0.1, 0.05::second) => env.set;
        //defGain => g.gain;
    }
    
    fun void init(Chubwrap chubby)
    {
        chubby @=> chub;
        chub => dac;
    }
    
        
    fun void init(Chugewrap chugey)
    {
        chugey @=> chuge;
        chuge => dac;
    }
    
    fun void init(StkInstrument inst) {
        inst @=> stk;
        stk => env => lpf => hpf => g => dac;
        //stk => bc => env => lpf => hpf => g => dac;
        //(.001::second , 0.2::second, 0.1, 0.05::second) => env.set;
        defGain => g.gain;
    }
    
    // set the time signature, bpm, and subdivisions of beat
    // bpm - BPM
    // N - time signature numerator (beats per measure)
    // noteBeats - value of the beats: 1 for quarter note, 0.5 for eigth note
    // sd - subdivision; what fraction of a beat do we want to increment by.
    fun void setTimeSig(float bpm, float N, float noteBeats, int sd) {
        bpm => BPM;
        N => tsN;
        sd => subdiv;
        (60/BPM * noteBeats)::second => beat;
        beat * tsN   => meas;
        // increment by tiny subdivisions of beat
        beat/subdiv => tick;                      
    }

    // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times
    fun void play(float p[], dur theTime, float Rept) {
        now + Rept * theTime => time tot;
        p @=> phrase;    
        
        //<<< "time to play">>>;
        1 => int times;
        while(now < tot && times <= Rept) {
            now + theTime => later;
            0 => currBeat;
            0 => curr;
            if(now < tot) {
               while(now < later) {                    
                    // the current rhythmic note                    
                    //phrase[curr] => currBeat;
                    if(curr == 0) {
                        phrase[curr]::second => now;                    
                        //<<< "first" >>>;
                        playBuf();
                    }
                    else if(curr > 0 && curr <= phrase.size()-1) {
                        (phrase[curr] - phrase[curr-1])::second => now;                        
                        //<<< "mid" >>>;
                        playBuf();
                    }                    
                    else if(curr > phrase.size() -1) {
                        theTime - (phrase[phrase.size()-1])::second => now;                        
                        //<<< "last" >>>;
                        break;
                    }
                    //<<< curr >>>;
                    curr++;
                    
                    
                    if(curr >= phrase.size())
                        curr % (phrase.size()+1) => curr;                          
                                       
                }    
            }    
            times++;  
        }
         
    }
    
    // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times
    fun void playChub(float p[], dur theTime, float Rept) {
        now + Rept * theTime => time tot;
        p @=> phrase;    
        
        //<<< "time to play">>>;
        1 => int times;
        while(now < tot && times <= Rept) {
            now + theTime => later;
            0 => currBeat;
            0 => curr;
            if(now < tot) {
               while(now < later) {                    
                    // the current rhythmic note                    
                    //phrase[curr] => currBeat;
                    if(curr == 0) {
                        phrase[curr]::second => now;                    
                        //<<< "first" >>>;
                        1 => chub.noteOn;
                    }
                    else if(curr > 0 && curr <= phrase.size()-1) {
                        (phrase[curr] - phrase[curr-1])::second => now;                        
                        //<<< "mid" >>>;
                         1 => chub.noteOn;
                    }                    
                    else if(curr > phrase.size() -1) {
                        theTime - (phrase[phrase.size()-1])::second => now;                        
                        //<<< "last" >>>;
                        break;
                    }
                    //<<< curr >>>;
                    curr++;
                    
                    
                    if(curr >= phrase.size())
                        curr % (phrase.size()+1) => curr;                          
                                       
                }    
            }    
            times++;  
        }
         
    }

    // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times
    fun void playChuge(float p[], dur theTime, float Rept) {
        now + Rept * theTime => time tot;
        p @=> phrase;    
        
        //<<< "time to play">>>;
        1 => int times;
        while(now < tot && times <= Rept) {
            now + theTime => later;
            0 => currBeat;
            0 => curr;
            if(now < tot) {
               while(now < later) {                    
                    // the current rhythmic note                    
                    //phrase[curr] => currBeat;
                    if(curr == 0) {
                        phrase[curr]::second => now;                    
                        //<<< "first" >>>;
                        1 => chuge.noteOn;
                    }
                    else if(curr > 0 && curr <= phrase.size()-1) {
                        (phrase[curr] - phrase[curr-1])::second => now;                        
                        //<<< "mid" >>>;
                        1 => chuge.noteOn;
                    }                    
                    else if(curr > phrase.size() -1) {
                        theTime - (phrase[phrase.size()-1])::second => now;                        
                        //<<< "last" >>>;
                        break;
                    }
                    //<<< curr >>>;
                    curr++;
                    
                    
                    if(curr >= phrase.size())
                        curr % (phrase.size()+1) => curr;                          
                                       
                }    
            }    
            times++;  
        }
         
    }
    
    // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times
    fun void play(float p[], dur theTime, float Rept) {
        now + Rept * theTime => time tot;
        p @=> phrase;    
        
        //<<< "time to play">>>;
        1 => int times;
        while(now < tot && times <= Rept) {
            now + theTime => later;
            0 => currBeat;
            0 => curr;
            if(now < tot) {
               while(now < later) {                    
                    // the current rhythmic note                    
                    //phrase[curr] => currBeat;
                    if(curr == 0) {
                        phrase[curr]::second => now;                    
                        //<<< "first" >>>;
                        playBuf();
                    }
                    else if(curr > 0 && curr <= phrase.size()-1) {
                        (phrase[curr] - phrase[curr-1])::second => now;                        
                        //<<< "mid" >>>;
                        playBuf();
                    }                    
                    else if(curr > phrase.size() -1) {
                        theTime - (phrase[phrase.size()-1])::second => now;                        
                        //<<< "last" >>>;
                        break;
                    }
                    //<<< curr >>>;
                    curr++;
                    
                    
                    if(curr >= phrase.size())
                        curr % (phrase.size()+1) => curr;                          
                                       
                }    
            }    
            times++;  
        }
         
    }
    
    // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times
    fun void playNotes(float p[], dur theTime, float Rept, float midi[]) {
        now + Rept * theTime => time tot;
        p @=> phrase;        
        //<<< "time to play">>>;
        1 => int times;
        while(now < tot && times <= Rept) {
            now + theTime => later;
            0 => currBeat;
            0 => curr;
            if(now < tot) {
               while(now < later) {                    
                    // the current rhythmic note
                    
                    //phrase[curr] => currBeat;
                    if(curr == 0) {
                        phrase[curr]::second => now;
                        midi[curr] => Std.mtof => osky.freq;
                        //<<< midi[curr] >>>;
                        //<<< "first" >>>;
                        1 => env.keyOn;
                    }
                    else if(curr > 0 && curr <= phrase.size()-1) {
                        (phrase[curr] - phrase[curr-1])::second => now;                        
                        midi[curr] => Std.mtof => osky.freq;
                        //<<< midi[curr] >>>;
                        //<<< "mid" >>>;
                        1 => env.keyOn;
                    }                    
                    else if(curr > phrase.size() -1) {
                        theTime - (phrase[phrase.size()-1])::second => now;                        
                        //<<< "last" >>>;
                        break;
                    }
                    //<<< curr >>>;
                    curr++;
                    
                    /*
                    if(curr >= phrase.size())
                        curr % (phrase.size()+1) => curr;                          
                    */
                    
                }    
            }    
            times++;  
        }
         
    }
    
     // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times
    fun void playNotes(float p[], float notesOFF[], dur theTime, float Rept, float midi[]) {
        now + Rept * theTime => time tot;
        p @=> phrase;
        spork ~ playOff(notesOFF, theTime, Rept);
        
        //<<< "time to play">>>;
        1 => int times;
        while(now < tot && times <= Rept) {
            now + theTime => later;
            0 => currBeat;
            0 => curr;
            if(now < tot) {
               while(now < later) {                    
                    // the current rhythmic note
                    
                    //phrase[curr] => currBeat;
                    if(curr == 0) {
                        phrase[curr]::second => now;
                        midi[curr] => Std.mtof => osky.freq;
                        //<<< midi[curr] >>>;
                        //<<< "first" >>>;
                        1 => env.keyOn;
                    }
                    else if(curr > 0 && curr <= phrase.size()-1) {
                        (phrase[curr] - phrase[curr-1])::second => now;                        
                        midi[curr] => Std.mtof => osky.freq;
                        //<<< midi[curr] >>>;
                        //<<< "mid" >>>;
                        1 => env.keyOn;
                    }                    
                    else if(curr > phrase.size() -1) {
                        theTime - (phrase[phrase.size()-1])::second => now;                        
                        //<<< "last" >>>;
                        break;
                    }
                    //<<< curr >>>;
                    curr++;
                    
                    /*
                    if(curr >= phrase.size())
                        curr % (phrase.size()+1) => curr;                          
                    */
                    
                }    
            }    
            times++;  
        }
         
    }
    
     // plays this rhythmic phrase 'p' for duration 'theTime', repeated 'Rept' times
    fun void playOff(float p[], dur theTime, float Rept) {
        now + Rept * theTime => time tot;
        p @=> phrase;        
        
        //<<< "time to play">>>;
        1 => int times;
        while(now < tot && times <= Rept) {
            now + theTime => later;
            0 => currBeat;
            0 => curr;
            if(now < tot) {
               while(now < later) {                    
                    // the current rhythmic note
                    
                    //phrase[curr] => currBeat;
                    if(curr == 0) {
                        phrase[curr]::second - 5::ms => now;   
                        <<< "stop" >>>;                 
                        //<<< "first" >>>;
                        1 => env.keyOff;
                        5::ms => now;
                    }
                    else if(curr > 0 && curr <= phrase.size()-1) {
                        (phrase[curr] - phrase[curr-1])::second - 1::ms => now;                        
                        <<< "stop" >>>;                 
                        //<<< "mid" >>>;
                        1 => env.keyOff;
                        5::ms => now;
                    }                    
                    else if(curr > phrase.size() -1) {
                        theTime - (phrase[phrase.size()-1])::second => now;                        
                        //<<< "last" >>>;
                        break;
                    }
                    //<<< curr >>>;
                    curr++;
                    
                    /*
                    if(curr >= phrase.size())
                        curr % (phrase.size()+1) => curr;                          
                    */
                    
                }    
            }    
            times++;  
        }
         
    }
    
    // lil helper function for playing SndBuf
    fun void playBuf() {
        1 => env.keyOn;
        0 => sb.pos;
    }

    // Takes a give phrase and swings it
    // midiPhrase[] - phrase to be swung (note Timings)
    // beat - what subdivision of the beat you want to be swung (16ths, 8ths)
    // swing - how hard to swing it!!
    fun static float[] swingIt(float midiPhrase[], dur beat, float swing) {
        float newPhrase[midiPhrase.size()];
        for(0 => int i; i < midiPhrase.size(); i++) {
            //<<< midiPhrase[i] >>>;
            midiPhrase[i] - 1 => newPhrase[i];
            //<<< "swung ? ", newPhrase[i] % 2 >>>;
            ((newPhrase[i] % 2) * (swing - 0.5) * 2 + newPhrase[i]) * beat/second => newPhrase[i];        
        }
        return newPhrase;
    }
    // takes a chord in the form of intervals (0, 5, 7, 10, etc.), 
    //   then adds the root to the intervals to get the appropriate MIDI number
    fun static float[] rtC(float chord[], float root) {
        float temp[chord.size()];
        for(0 => int i; i < temp.size(); i++) {
            chord[i] + root => temp[i];
        }
        return temp;    
    }
    // try to get noteOffs by calculating note length, but something DOESNT WORK
    fun static float[] noteLengths(float notes[], float durs[]) {
        float temp[notes.size()];
        for(0 => int i; i < temp.size(); i++) {
            notes[i] + durs[i] => temp[i];
        }
        return temp;  
    }
    // shift notes from human notation (1 - n) to computer array notation (0 - n-1)
    fun static float[] noteShift(float notes[], dur beat) {
        float temp[notes.size()];
        for(0 => int i; i < notes.size(); i++) {
            (notes[i] - 1) * beat/second => temp[i];
        }
        return temp;
    }
    
}