//chout <= "i should be running...." <= IO.newline();
ImpReson ipr => Gain gain => dac;
//Gain gain;

// for initializing
external float freq;
external float note;
external Event init;
// for playing
external float strikePos, vel;
external Event struck;
//chout <= "initialized chime parameters" <= IO.newline();

// wait for parameters from Unity
init => now;
//chout <= "note" <= IO.newline();
//chout <= note <= IO.newline();
note => Std.mtof => freq;
//chout <= "freq" <= IO.newline();
//chout <= freq <= IO.newline();
freq => ipr.freq;

while(true) {
    struck => now;
    //chout <= vel <= IO.newline();
    //chout <= strikePos <= IO.newline();
    
    spork ~ ipr.strike(strikePos, vel);
    
}  
//30::second => now;
//theChime.
