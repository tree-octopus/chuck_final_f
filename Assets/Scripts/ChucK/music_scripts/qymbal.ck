class FSQ extends Chubgraph
{
    SqrOsc q => Gain g => outlet;
    
    SqrOsc mod => g;
    3 => g.op;
    int seed;

    fun void freq(float f)
    {
        f => q.freq;
    }
    
    fun float getFreq()
    {
        return q.freq();
    }
    
    fun void modFreq(float m)
    {
        m => mod.freq;
    }
    
    fun float getModFreq()
    {
        return mod.freq();
    }    
}


public class Qymbal extends Chubwrap
{
    FSQ oscs[10];
    ExpEnv env => BPF bpf => HPF hpf => outlet;
    (0::ms, 200::ms, 0, 0::ms) => env.set;
    10500 => bpf.freq;
    4000 => hpf.freq;
    1 => hpf.Q;
    1 => bpf.Q;
    // to keep things consistent!
    150 => int seed;
    seed => Math.srandom;
    
    for(0 => int i; i < oscs.cap(); i++)
    {
        oscs[i] => env;
        Math.random2f(1000, 2000) => oscs[i].freq;
        Math.random2f(200, 400) => oscs[i].modFreq;
        //2 * oscs[i].getFreq() + oscs[i].getModFreq() => oscs[i].bpf.freq;
        1./oscs.cap() => oscs[i].gain;

    }
    
    fun void noteOn(int no)
    {
        //<<< "a" >>>;
        no => env.setKeyOn;
    }    
}

/*
Qymbal cq => dac;
//(30::ms, 1600::ms, 0, 0::ms) => cq.env.set;
1 => cq.noteOn;
/*
FSQ f => dac;

10 => f.bpf.Q;
Math.random2f(300, 800) => f.freq;
Math.random2f(100, 300) => f.modFreq;
3 * f.getFreq() + f.getModFreq() => f.bpf.freq;
*/
//2::second => now;
