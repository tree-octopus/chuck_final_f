// final proj - Jeff Huang
// bass

int wait;
// print out all arguments
if(me.args() > 0)
{
    // how many measures to wait before starting to play!
    me.arg(0) => Std.atoi => wait;
}

// initialize - starting BPM and stuff
// ================================================== //
79 => float BPM;
// durations in musical time
(60./BPM)::second => dur qt;
qt*4. => dur meas;
qt/2. => dur et;
time later;

// initialize instruments
// ================================================== //
quant bass;
bass.init();
(6::ms, 250::ms, 0., 50::ms) => bass.env.set;
0.15 => bass.g.gain;
120 => bass.hpf.freq;
1.5 => bass.hpf.Q;
300 => bass.lpf.freq;

// Sending OSC for visuals
//----------------------//
40000 => int outPort;
OscWrapper bass_out;
bass_out.send.setHost("localhost", outPort);
bass_out.setTimeSig(79, 4, 1, 1000);
"/MirrorOffset" => string bass_add;
bass_add => bass_out.msg;
//----------------------//

// set stuff
// ================================================== //
bass.setTimeSig(BPM, 4, 1, 1000);

26. => float root;
// bass phrases
[0.* bass.beat/second, 0.6 * bass.beat/second, 1.2 * bass.beat/second, 
 2 * bass.beat/second, 2.25 * bass.beat/second, 2.8*bass.beat/second,
 3.5 * bass.beat/second] @=> float timesA[];
    
quant.rtC([0., 5., 10., 5., 7., 10., 3.], root) @=> float pitchesA[];

[0.* bass.beat/second, 
    1.5 * bass.beat/second, 1.8 * bass.beat/second, 
    2.4 * bass.beat/second, 
    3 * bass.beat/second, 3.4 * bass.beat/second, 3.8 * bass.beat/second,
 (0 + 4.) * bass.beat/second, 
    (1.5 + 4.) * bass.beat/second, (1.75 + 4.) * bass.beat/second, 
    (2. + 4.) * bass.beat/second, (2.4 + 4.) * bass.beat/second, (2.8 + 4.) * bass.beat/second,
    (3.25 + 4.) * bass.beat/second, (3.6 + 4.) * bass.beat/second,
 (0 + 8.) * bass.beat/second,
    (1.4 + 8.) * bass.beat/second, (1.7 + 8.) * bass.beat/second,
    (2. + 8.) * bass.beat/second, (2.4 + 8.) * bass.beat/second, (2 + 2./3 + 8.) * bass.beat/second,
    (3. + 8.) * bass.beat/second, (3.4 + 8.) * bass.beat/second,(3.75 + 8.) * bass.beat/second,
 (0 + 12.) * bass.beat/second, 
    (1.4 + 12.) * bass.beat/second, (1.8 + 12.) * bass.beat/second, 
    (2. + 12.) * bass.beat/second, (2. + 1/3. + 12.) * bass.beat/second, (2. + 2/3. + 12.) * bass.beat/second, 
    (3. + 12.) * bass.beat/second, (3. + 1/3. + 12.) * bass.beat/second, (3. + 2/3. + 12.) * bass.beat/second] @=> float timesB[];
    
quant.rtC([0., 5., 7., 0., 5., 7., 10.,
           2., 2+10., 2+11., 2+12., 2+7., 2+0., 2+7., 2+6., 
           7., 7+3., 7+6., 7+8., 7+10., 7+8., 7+6., 7-2., 7+3.,
           9., 9+7., 9+9., 9+10., 9+9., 9+7., 9+6., 9+0., 9-6.], root) @=> float pitchesB[];
           
<<< "times:", timesB.cap(), ", pitches:", pitchesB.cap() >>>;           


// play stuff
// ================================================== //

now + wait*bass.meas => later;
/*
while(now < later) {
    bass.meas => now;
}
*/
wait*bass.meas => now;
// A

spork ~ bass.playNotes(timesA, 1*bass.meas, 12, pitchesA);
spork ~ bass_out.sendRhythm(timesA, 1*bass.meas, 12);
12*bass.meas => now;

// B
spork ~ bass.playNotes(timesB, 4*bass.meas, 5, pitchesB);
spork ~ bass_out.sendRhythm(timesB, 4*bass.meas, 5);
20*bass.meas => now;
/*
now + 20*bass.meas => later;
while(now < later) {
    bass.meas => now;
}
*/
//spork ~ bass.playNotes([0. * bass.meas/second, 1. * bass.meas/second, 2. * bass.meas/second], 3.5*bass.meas, 1, [root, root - 2, root - 11]);
now + 3.5*bass.meas => later;
(6::ms, 5000::ms, 1., 50::ms) => bass.env.set;
spork ~ bass_out.sendRhythm([0. * bass_out.beat/second], 1*bass.meas, 1);
root-2 => Std.mtof => bass.osky.freq;
1 => bass.env.keyOn;
bass.meas => now;
1 => bass.env.keyOff;
spork ~ bass_out.sendRhythm([0. * bass_out.beat/second], 1*bass.meas, 1);
root => Std.mtof => bass.osky.freq;
1 => bass.env.keyOn;
bass.meas => now;
1 => bass.env.keyOff;
spork ~ bass_out.sendRhythm([0. * bass_out.beat/second], 1*bass.meas, 1);
root+1 => Std.mtof => bass.osky.freq;
1 => bass.env.keyOn;
bass.meas => now;
1 => bass.env.keyOff;


