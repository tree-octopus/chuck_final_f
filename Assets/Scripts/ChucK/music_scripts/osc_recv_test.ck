// create our OSC receiver
OscRecv recv;
6969 => int inPort;
inPort => recv.port;
// start listening (launch thread)
recv.listen();

"/bilbo, f" => string numEvent;
// create an address in the receiver, store in new variable
recv.event( numEvent ) @=> OscEvent oe;

while(true)
{
    <<< "waiting" >>>;
    
    oe => now;
    
    chout <= "got something" <= IO.newline();
    while ( oe.nextMsg() != 0 )
    {
        chout <= "wow" <= IO.newline();
        oe.getFloat() => float billy;
        chout <= billy <= IO.newline();
    }    
}
