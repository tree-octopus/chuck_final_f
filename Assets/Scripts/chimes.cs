﻿using UnityEngine;
using System.IO;
using System.Collections;

public class chimes : MonoBehaviour {

    public GameObject chimeFab;

    float numChimes;
    public float bassNote;
    public float[] chimeTunings;    

    string bwg_path;
    string chimes_class;
    string chimes_runner;
    Chuck.FloatCallback[] chimeNotes = null;

    // callback testing
    Chuck.FloatCallback test_callback = null;
    double test_float;
    string test_path;

    // OSC junk
    public OSC osc;
    

	void Start () {
        //chuckPaths();

        /*
        test_path = Application.dataPath + "/Scripts/ChucK/callback_test.ck";
        Debug.Log(test_path);
        GetComponent<ChuckInstance>().RunCode(File.ReadAllText(test_path));
        */
        test_path = Application.dataPath + "/Scripts/ChucK/score.ck";
        Debug.Log(test_path);
        GetComponent<ChuckInstance>().RunCode(File.ReadAllText(test_path));
        
        //StartCoroutine(chuckPaths());

        initOSCJunk();
        numChimes = chimeTunings.Length;
        Invoke("sendChimeNum",2);
        //sendChimeNum();
        //sendChimeTunes();
        // GameObject temp = Instantiate(chimeFab, gameObject.transform.InverseTransformPoint(gameObject.transform.position), Quaternion.identity);
        //temp.transform.SetParent(gameObject.transform, false);
       
        //temp.transform.parent = gameObject.transform;


    }
	
    void initOSCJunk()  
    {
        osc.SetAddressHandler("/test", getTestOSC);
        osc.SetAddressHandler("/ARG", getARG);
        Debug.Log("initted osc junk");
    }

    IEnumerator chuckPaths()
    {
        /*
        test_path = Application.dataPath + "/Scripts/ChucK/testers_of_test.ck";
        Debug.Log(test_path);
        GetComponent<ChuckInstance>().RunCode(File.ReadAllText(test_path));
        */
        test_path = Application.dataPath + "/Scripts/ChucK/OscRecv_test.ck";
        Debug.Log(test_path);
        GetComponent<ChuckInstance>().RunCode(File.ReadAllText(test_path));
        yield return new WaitForSeconds(.1f);
    }

    void spawnChimes()
    {
        Vector3 pos = gameObject.transform.position;
        Vector3 newPos;
        GameObject temp;
        for(int i = 0; i < numChimes; i++)
        {
            newPos = new Vector3 (0, (i + 1) * 2f / (numChimes + 1) + -1, 0);
            
            temp = Instantiate(chimeFab, newPos, Quaternion.identity);            
            temp.transform.SetParent(gameObject.transform, false);            
            temp.transform.GetChild(0).transform.localScale = new Vector3(1,chimeTunings[i],1);
            
            SoftJointLimit sjl = new SoftJointLimit();
            sjl.limit = 0.2f + chimeTunings[i]*.2f;            

            temp.transform.GetChild(0).GetComponent<ConfigurableJoint>().linearLimit = sjl;
           
            //Debug.Log(temp.transform.GetChild(0).transform.position);
            bilbo = temp;
        }        
    }

    GameObject bilbo;

	// Update is called once per frame
	void Update () {
        
        /*
        if (test_callback == null)
        {
            Debug.Log("??");
            test_callback = GetComponent<ChuckInstance>().GetFloat("test", getTest);
            Debug.Log(test_callback);
            Debug.Log("?@!");
        }
        */
    }

    void getTestOSC(OscMessage msg)
    {
        Debug.Log("hey we got OSC");
        double[] test = new double[3];
        for(int i = 0; i < 3; i++)
        {
            test[i] = msg.GetFloat(i);
        }
        //Debug.Log(test);
        for (int i = 0; i < 3; i++)
        {
            Debug.Log(test[i] + "!");
        }
        spawnChimes();
    }

    void getARG(OscMessage msg)
    {
        Debug.Log("got a " + msg.GetFloat(0));
        if (msg.GetFloat(0) == 2)
        {
            Debug.Log("gonna do this thing");
            sendChimeTunes();
        }

    }

    void sendChimeNum()
    {
        Debug.Log("I'm gonna send some stuff");
        OscMessage msg = new OscMessage();
        msg.address = "/numChimes";
        msg.values.Add(numChimes);
        /*
        msg.values.Add(bassNote);
        for (int i = 0; i < numChimes; i++)
        {
            msg.values.Add(chimeTunings[i]);
        }
        */
        osc.Send(msg);
        //Debug.Log("sent stuff "+msg.address);      
    }

    void sendChimeTunes()
    {
        Debug.Log("sending chime tunes");
        OscMessage msg = new OscMessage();
        msg.address = "/chimeTunes";        
        msg.values.Add(bassNote);
        for (int i = 0; i < numChimes; i++)
        {
            msg.values.Add(chimeTunings[i]);
        }
        osc.Send(msg);
    }

    void getTest( double tester )
    {
        Debug.Log("why");
        test_float = tester;
        test_callback = null;
        Debug.Log("test float: " + test_float);
    }
}
