﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public class music : MonoBehaviour {

    string script_path;

	// Use this for initialization
	void Start () {
        script_path = Application.dataPath + "/Scripts/ChucK/music_scripts/final_proj_score.ck";
        //Debug.Log(script_path);
        GetComponent<ChuckInstance>().RunCode(File.ReadAllText(script_path));
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
