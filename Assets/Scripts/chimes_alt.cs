﻿using UnityEngine;
using System.IO;
using System.Collections;

public class chimes_alt : MonoBehaviour {

    public GameObject chimeFab;

    int numChimes;
    public float rootNote;
    public float[] chimeTunings;    

    string bwg_path;
    string chimes_class;
    string chimes_runner;
    Chuck.FloatCallback[] chimeNotes = null;

    // callback testing
    Chuck.FloatCallback test_callback = null;
    double test_float;
    string test_path;

    public float length_param = 8000;

    // OSC junk
    public OSC osc;

    string script_path;

    private void Awake()
    {
        //script_path = Application.dataPath + "/Scripts/ChucK/music_scripts/score.ck";
        //Debug.Log(script_path);
        //GetComponent<ChuckInstance>().RunCode(File.ReadAllText(script_path));
        
    }

    void Start () {
        //Debug.Log("starting");
        numChimes = chimeTunings.Length;
        spawnChimes();
    }
	

    void spawnChimes()
    {
        Vector3 pos = gameObject.transform.position;
        Vector3 newPos;
        GameObject temp;
        GameObject temp_child;
        GameObject[] chimes = new GameObject[numChimes];
        for (int i = 0; i < numChimes; i++)
        {
            Debug.Log("makign stuff");
            newPos = new Vector3 (0, (i + 1) * 2f / (numChimes + 1) + -1, 0);

            temp = Instantiate(chimeFab, newPos, Quaternion.identity);
            chimes[i] = temp;
            temp.transform.SetParent(gameObject.transform, false);
            temp_child = temp.transform.GetChild(0).gameObject;

            //temp_child.GetComponent<single_chime>().chucky = this.GetComponent<ChuckInstance>();

            temp_child.GetComponent<single_chime>().lengthParam = length_param;
            temp_child.GetComponent<single_chime>().chime_root = rootNote;
            temp_child.GetComponent<single_chime>().chime_interval = chimeTunings[i];
            //temp.transform.GetChild(0).transform.localScale = new Vector3(1,temp_child.GetComponent<single_chime>().calcLength(),1);
            
            /*
            SoftJointLimit sjl = new SoftJointLimit();
            sjl.limit = 0.2f + temp_child.transform.localScale.y;            

            temp.transform.GetChild(0).GetComponent<ConfigurableJoint>().linearLimit = sjl;
            */
           
            

            //Debug.Log(temp.transform.GetChild(0).transform.position);
            bilbo = temp;
        }
        for(int i = 0; i < numChimes; i++)
        {
            temp = chimes[i];
            temp_child = temp.transform.GetChild(0).gameObject;

            SoftJointLimit sjl = new SoftJointLimit();
            sjl.limit = 0.2f + temp_child.transform.localScale.y;

            temp.transform.GetChild(0).GetComponent<ConfigurableJoint>().linearLimit = sjl;
            /*
            temp = chimes[i];
            temp.transform.GetChild(0).transform.localPosition += new Vector3(100, 0, 0);
            Debug.Log("offsetting");
            */

        }
    }

    GameObject bilbo;

	// Update is called once per frame
	void Update () {

    }

}
