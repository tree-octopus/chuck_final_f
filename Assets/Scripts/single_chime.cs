﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class single_chime : MonoBehaviour {

    string script_path;
    public float chime_root;
    public float chime_interval;

    Chuck.FloatCallback freqCallback = null;
    public double myFreq;

    public float lengthParam = 8000;
    public float length;

    //public ChuckInstance chucky;

    private void Awake()
    {
        script_path = Application.dataPath + "/Scripts/ChucK/music_scripts/score.ck";
        //Debug.Log(script_path);
        GetComponent<ChuckInstance>().RunCode(File.ReadAllText(script_path));
        //Debug.Log("running ChucK script!");       
        
    }

    // Use this for initialization
    void Start () {
        //SetChimeLength();

    }

    public float calcLength()
    {
        length = Mathf.Sqrt(lengthParam / ((float)myFreq));
        return length;
    }

    public void SetChimeLength()
    {
        //Debug.Log("SETTING THE CHIME");
        transform.localScale = new Vector3(1, calcLength(), 1);
        GetComponent<Rigidbody>().SetDensity(8); 
    }


    void getFreq( double freq)
    {
        //Debug.Log("setting the freq: "+freq);
        this.myFreq = freq;
        if(myFreq != 0)
            SetChimeLength();
    }

    bool freq_set = false;

    // Update is called once per frame
    void Update () {
        if(freqCallback == null)
        {
            //Debug.Log("it was null");
            freqCallback = GetComponent<ChuckInstance>().CreateGetFloatCallback(getFreq);
            
            //Debug.Log("root: " + chime_root + ", interval: " + chime_interval);
            GetComponent<ChuckInstance>().SetFloat("note", chime_interval + chime_root);
            //Debug.Log("i should have sent stuff, but???");
            //GetComponent<ChuckInstance>().BroadcastEvent("init");
            //GetComponent<ChuckInstance>().GetFloat("freq", getFreq);
            //SetChimeLength();
        }
        else if (myFreq == 0)
        {
            GetComponent<ChuckInstance>().BroadcastEvent("init");
            GetComponent<ChuckInstance>().GetFloat("freq", getFreq);

            //SetChimeLength();
        }
        else if (myFreq != 0 && !freq_set)
        {
            SetChimeLength();
            freq_set = true;
        }
        //SetChimeLength();
    }

    private void OnCollisionEnter(Collision collision)
    {
        Vector3 touch_point = gameObject.transform.InverseTransformPoint(collision.contacts[0].point);
        //Debug.Log("you touched the butt");
        float strikePos = clamp((touch_point.y + 1) / 2,0,1);
        GetComponent<ChuckInstance>().SetFloat("strikePos", strikePos);
        float vel = clamp(collision.relativeVelocity.magnitude, 0, 1);
        if (vel <= 0)
            vel = 0;
        //Debug.Log("strikePos: " + strikePos);
        //Debug.Log("velocity: " + vel);
        GetComponent<ChuckInstance>().SetFloat("vel", vel);

        GetComponent<ChuckInstance>().BroadcastEvent("struck");
    }

    float clamp(float k, float min, float max)
    {
        return Mathf.Max(min, Mathf.Min(k, max));
    }
}
