﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour {

    public float xRot, yRot, zRot;
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.up * xRot* Time.deltaTime);
        transform.Rotate(Vector3.right * yRot * Time.deltaTime);
        transform.Rotate(Vector3.forward * zRot * Time.deltaTime);
    }
}
