﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kino;

public class texture_mod : MonoBehaviour {

    float def_tile_x = 5;
    float def_tile_y = 3;
    // Use this for initialization

    public OSC osc;
    public Camera tex_cam;
    public Camera main_cam;
    public Mirror mirror;
    public Light light;
    public Material matty;    

    float framesPerBeat;

    Color transp = new Color(1, 1, 1, 0);
    Color opaque = new Color(1, 1, 1, 1);

    void Start () {
        //GetComponent<Renderer>().material.mainTextureScale = new Vector2(def_tile_x, def_tile_y);
        GetComponent<Renderer>().material.color = transp;
        osc.SetAddressHandler("/TexFadeIn", TexFadeIn);
        osc.SetAddressHandler("/TexFadeOut", TexFadeOut);
        osc.SetAddressHandler("/TexCamRot", TexCamRot);
        osc.SetAddressHandler("/MirrorRoll", MirrorRoll);
        osc.SetAddressHandler("/MirrorSymm", MirrorSymm);
        osc.SetAddressHandler("/LightColor", LightColor);
        osc.SetAddressHandler("/MirrorOffset", MirrorOffset);
        osc.SetAddressHandler("/Displacement", Displacement);
        osc.SetAddressHandler("/DispOff", DispOff);
        osc.SetAddressHandler("/ITSOVER", OVER);
        //Debug.Log(Time.fixedDeltaTime);
        framesPerBeat = (1.0f / (Time.fixedDeltaTime)) * (60.0f / BPM);

        x_rot = tex_cam.transform.eulerAngles.x;
        y_rot = tex_cam.transform.eulerAngles.y;

        matty.SetFloat("_Magnitude", 0);
        mirror._repeat = Mathf.Clamp(dis_count, 0, 360);
    }

    Color temp;
    float lerpy;
	// Update is called once per frame
	void Update () {

        //Debug.Log(temp);
        if(fad == 1)
        {
            if (count <= framesPerBeat * 2)
            {
                count++;
                lerpy = count / (framesPerBeat * 2);
                GetComponent<Renderer>().material.color = Color.Lerp(transp, opaque, lerpy);
            }
        }
        else if (fad == 2)
        {
            if (count <= framesPerBeat * 2)
            {
                count++;
                lerpy = count / (framesPerBeat * 2);
                GetComponent<Renderer>().material.color = Color.Lerp(opaque, transp, lerpy);
                mirror._repeat = (int) (old_rept * (1-lerpy));
                mirror._roll = old_roll * (1-lerpy);
                mirror._offset = old_off * (1-lerpy);
            }
        }

        if(isItColorTime)
        {
            color_counter++;
            color_counter %= (int) Mathf.Round(framesPerBeat * 4);
            color_lerpy = color_counter / (framesPerBeat * 4.0f);
            Debug.Log(light.color);
            light.color = Color32.Lerp(colors[color_cyc], colors[(color_cyc + 1) % colors.Length], color_lerpy);
        }
        //tex_cam.transform.eulerAngles = new Vector3(0, 0, z_rot++);
        if (displace)
        {
            dis_count++;
            dis_count %= (int)Mathf.Round(framesPerBeat * 8);
            matty.SetFloat("_Magnitude", Mathf.PingPong(dis_count / (framesPerBeat * 8), 0.1f) );
            mirror._repeat = Mathf.Clamp(dis_count, 0, 360);
        }
        else if(!displace && !theEnd)
        {
            dis_count--;
            dis_count %= (int)Mathf.Round(framesPerBeat * 8);
            dis = dis_count / (framesPerBeat * 8) * 0.1f;
            matty.SetFloat("_Magnitude", dis);
            Debug.Log(dis);
            if (dis <= 0.001)
                theEnd = true;
            mirror._repeat = Mathf.Clamp(dis_count, 0, 360);
        }
        else if(theEnd && !displace)
        {
            //mirror._repeat = 0;
            matty.SetFloat("_Magnitude", 0);
            //mirror._repeat = 0;
            //mirror._offset = 0;
            //mirror._roll = 0;
        }
        else if(STOP)
        {
            //mirror._repeat = 0;
            //mirror._offset = 0;

            //mirror._roll = 0;
            //fad = 2;
        }
        
    }

    float x_rot, y_rot, z_rot;

    float BPM = 79;
    int fad = 0;
    int count;

    #region OSC Handling stuff

    void TexFadeIn(OscMessage msg)
    {
        fad +=(int) msg.GetFloat(0);
        //.Log("!!!!!: "+fad);
        count = 0;
    }

    void TexFadeOut(OscMessage msg)
    {
        fad += (int)msg.GetFloat(0);
        count = 0;
    }

    void TexCamRot(OscMessage msg)
    {
        //Debug.Log("got drums");
        z_rot += 6;
        //if (z_rot % 2 == 0)
           // mirror._repeat = Random.Range(3, 9);
        tex_cam.transform.eulerAngles = new Vector3(x_rot, y_rot, z_rot);
    }

    bool ping_pong;

    void MirrorRoll(OscMessage msg)
    {
        mirror.enabled = true;
        // Debug.Log("got shakes");
        if (z_rot % 2 == 0)
         mirror._repeat = Random.Range(3, 9);
        mirror._roll += 6;
    }

    void MirrorSymm(OscMessage msg)
    {
        mirror._symmetry = !mirror._symmetry;
    }

    int color_cyc, color_counter;
    Color[] colors = {new Color32(181, 56, 209, 255), new Color32(252, 215, 53, 255), new Color32(11, 24, 142, 255), new Color32(234, 28, 121, 255)};
    bool isItColorTime = false;
    float color_lerpy;
    void LightColor(OscMessage msg)
    {
        //Debug.Log("COLORS");
        isItColorTime = true;
        color_cyc++;
        color_cyc %= 4;
    }

    void MirrorOffset(OscMessage msg)
    {
        mirror._offset -= 6;
    }

    bool displace = false;
    int dis_count;
    float dis_lerp, dis;
    void Displacement(OscMessage msg)
    {
        displace = true;
        matty.SetFloat("_Magnitude", Random.Range(0, 0.1f));
        mirror._symmetry = true;
    }
    bool theEnd = false;
    void DispOff(OscMessage msg)
    {
        displace = false;
        Debug.Log("stop displacing!!");
        dis_count = (int)Mathf.Round(framesPerBeat * 8) - 1;
    }

    bool STOP = false;
    float old_off, old_roll;
    int old_rept;
    void OVER(OscMessage msg)
    {
        //mirror._repeat = 0;
        STOP = true;
        Debug.Log("stop now");
        old_rept = mirror._repeat;
        old_off = mirror._offset;
        old_roll = mirror._roll;

        //mirror._roll = 0;
        count = 0;
        fad = 2;
        mirror._symmetry = false;
    }
    #endregion
}
