﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rigidbody_wind : MonoBehaviour {
    List<Rigidbody> windBodies = new List<Rigidbody>();

    
    public GameObject dir;
    public Vector3 windDirection = Vector3.right;

    public float windStrength = 5;
    public bool periodic = false;
    public float freq, phase, offset, jitter;

	// Use this for initialization
	void Start () {
        //windDirection = Vector3.forward;
        //windDirection = dir.transform.Rotate(dir.transform.rotation.eulerAngles, windDirection);
        //Vector3.RotateTowards();
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(windBodies.Count);
	}

    private void OnTriggerEnter(Collider other)
    {
        Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
        if (rb != null)
            windBodies.Add(rb);
    }

    private void OnTriggerExit(Collider other)
    {
        Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
        if (rb != null)
            windBodies.Remove(rb);
    }

    private void FixedUpdate()
    {
        if (windBodies.Count > 0)
        {
            if(!periodic)
            {
                foreach (Rigidbody rb in windBodies)
                {
                    rb.AddForce(windDirection.normalized * (windStrength + jitter * Random.Range(0, 1)));
                }
            }
            else
            {
                foreach (Rigidbody rb in windBodies)
                {
                    rb.AddForce(windDirection.normalized * (windStrength * sinLFO(Time.deltaTime * i, freq, phase) + offset + jitter * Random.Range(-1, 1)));                    
                }
            }
            i++;
        }
    }

    int i = 0;

    float sinLFO(float t, float freq, float phase)
    {
        return Mathf.Sin(2 * Mathf.PI * t * freq + phase);
    }
}
