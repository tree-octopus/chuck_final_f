﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class that_script : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [SerializeField] Shader _shader;
    [SerializeField] Material _material;

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        //destination = _material;
        if(_material != null)
            Graphics.Blit(source, destination, _material);
    }
}
