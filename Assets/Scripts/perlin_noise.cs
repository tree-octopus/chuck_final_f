﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class perlin_noise : MonoBehaviour {

    public int pixWidth;
    public int pixHeight;
    public float xOrg;
    public float yOrg;
    public float scale = 1.0F;
    public Texture2D noiseTex;
    private Color[] pix;
    private Renderer rend;

    public float xRate, yRate;

    // Use this for initialization
    void Start () {
        rend = GetComponent<Renderer>();
        noiseTex = new Texture2D(pixWidth, pixHeight);
        pix = new Color[noiseTex.width * noiseTex.height];
        rend.material.mainTexture = noiseTex;
    }

    void CalcNoise()
    {
        float y = 0.0F;
        while (y < noiseTex.height)
        {
            float x = 0.0F;
            while (x < noiseTex.width)
            {
                float xCoord = xOrg + x / noiseTex.width * scale;
                float yCoord = yOrg + y / noiseTex.height * scale;
                float sample = Mathf.PerlinNoise(xCoord, yCoord);
                pix[(int)(y * noiseTex.width + x)] = new Color(sample, sample, sample);
                x++;
            }
            y++;
        }
        noiseTex.SetPixels(pix);
        noiseTex.Apply();
    }

    // Update is called once per frame
    void Update () {
        xOrg += xRate;
        xOrg %= 50;
        yOrg += yRate;
        yOrg %= 50;
        CalcNoise();
    }
}
