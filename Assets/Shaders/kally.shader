﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/kally" 
{
	Properties 
	{
		
		_MainTex ("Texture", 2D) = "white"	{}
		_Color("Color", Color) = (1,1,1,1)
		
	}
	SubShader 
	{
		Tags
		{
			"Queue" = "Transparent"
		}

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _Color;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				return o;
			}

			
			float4 frag(v2f i) : SV_Target
			{
				//float4 color = float4(i.uv.x, i.uv.y, 1, 1);
				//i.uv.x *= 4;
				//i.uv.y *= 4;
				//i.uv.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				float4 color;
				/*
				if(i.uv.x >= 1)
				{
					//color = tex2D(_MainTex, i.uv);
					i.uv.x = -i.uv.x+3;
				}
				else
				{
					//color = tex2D(_MainTex, i.uv);
				}
				if(i.uv.y >= 1)
				{
					i.uv.y = -i.uv.y+3;
				}
				else
				{
				
				}
				*/
				color = tex2D(_MainTex, i.uv);
				color *= _Color;
				return color;
			}
			
			ENDCG
		}
		

		
	}

}
