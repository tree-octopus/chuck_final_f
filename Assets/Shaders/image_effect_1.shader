﻿Shader "Hidden/image_effect_1"
{
	Properties
	{
		_MainTex ("-", 2D) = "" {}
	}
	CGINCLUDE

	#pragma multi_compile _SYMMETRY_ON

	sampler2D _MainTex;
	float _Divisor;
	float _Offset;
	float _Roll;

	half4 vertex_coord;

	void main()
	{
		//vertex_coord = gl_Vertex;
		//gl_Position = gl_Vertex;

		//glRecti(-1,-1,1,1);
	}

	
	ENDCG
	
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			//sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				// just invert the colors
				col = 1 - col;
				//return col;
				return tex2D(_MainTex, i.uv);
			}
			ENDCG
		}
	}
	
}
